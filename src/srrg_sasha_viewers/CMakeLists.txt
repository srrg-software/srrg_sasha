add_library(srrg_sasha_viewers_library SHARED
  shapes_viewer.cpp shapes_viewer.h
  tracker_viewer.cpp tracker_viewer.h
  )

target_link_libraries(srrg_sasha_viewers_library
  srrg_sasha_library
  ${catkin_LIBRARIES}
  ${QGLVIEWER_LIBRARY} 
  ${SRRG_QT_LIBRARIES} 
  ${OPENGL_gl_LIBRARY} 
  ${OPENGL_glu_LIBRARY}
)
