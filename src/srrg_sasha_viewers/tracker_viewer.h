#pragma once
#include <srrg_types/types.hpp>

#include <qapplication.h>
#include <qglviewer.h>
#include <QKeyEvent>
#include <srrg_sasha/shapes_tracker.h>

class QPaintEvent;
class QPainter;


#if QT_VERSION >= 0x050000
typedef qreal qglviewer_real;
#else
typedef float qglviewer_real;
#endif

namespace srrg_sasha{

  typedef std::vector<Eigen::Isometry3f,
    Eigen::aligned_allocator<Eigen::Isometry3f> > Isometry3fVector;
  
  class StandardCamera: public qglviewer::Camera {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  protected:

    virtual qglviewer_real zFar() const { return 100; }
    virtual qglviewer_real zNear() const { return 0.1; }
  };

  class TrackerViewer : public QGLViewer {
  public:
    TrackerViewer() {


      StandardCamera * cam =new StandardCamera();
      setCamera(cam);

      srrg_core::Vector6f v;
      v << 0.0f,0.0f,0.0f,-0.5f,0.5f,-0.5f;
      Eigen::Isometry3f t = srrg_core::v2t(v);

      Eigen::Vector3f pos (0,0,-2);
      Eigen::Vector3f dir = t.rotation()*Eigen::Vector3f(0,-1,0);
      cam->setPosition(qglviewer::Vec(pos.x(),pos.y(),pos.z()));
      cam->setUpVector(qglviewer::Vec(0.0f, -1.0f, 0.0f));
      cam->lookAt(qglviewer::Vec(pos.x()+dir.x(),pos.y()+dir.y(),pos.z()+dir.z()));

      _tracker = NULL;
      _scene_fixed = NULL;
      _play = false;
      _forward = false;
      _follow_camera = false;
      _save_snapshot = false;
      _current_time = srrg_core::getTime();
      _camera_offset.setIdentity();
      _camera_poses.resize(1000, Eigen::Isometry3f::Identity());
      _to_draw_cameras = 0;

//      QLinearGradient gradient(QPointF(50, -20), QPointF(80, 20));
//      gradient.setColorAt(0.0, Qt::white);
//      gradient.setColorAt(1.0, QColor(0xa6, 0xce, 0x39));

//      textPen = QPen(Qt::black);
//      textFont.setPixelSize(20);
    }
    void init() {
      QColor white = QColor(Qt::white);
      setBackgroundColor(white);


      const GLfloat pos[4] = {0.f, -1.f, -1.f, 1.0f};
      glLightfv(GL_LIGHT1, GL_POSITION, pos);
      glDisable(GL_LIGHT0);
      glEnable(GL_LIGHT1);

      const GLfloat light_ambient[4] = {.5f,.5f,.5f, .5f};
      glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);

      const GLfloat light_diffuse[4] = {1.0, 1.0, 1.0, 1.0};
      glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
    }

    inline bool& play() {return _play;}
    inline bool& forward() {return _forward;}
    inline bool& saveSnap() {return _save_snapshot;}
    
    virtual void draw();

    void setTracker(ShapesTracker* tracker_){
      _tracker = tracker_;
      _scene_fixed = _tracker->sceneFixed();
      _scene_moving = _tracker->sceneMoving();
      _matchable_store = _tracker->matchableStore();
    }

    Isometry3fVector& cameraPoses() {return _camera_poses;}
    int& toDrawCameras() {return _to_draw_cameras;}
    
    void drawFloor();

//    void drawOverpaint(QPainter *painter);
//    virtual void paintGL() { update(); }
//    virtual void paintEvent(QPaintEvent *event);

  protected:
    virtual void  keyPressEvent(QKeyEvent *event);
    
  protected :
    ShapesTracker* _tracker;
    Eigen::Isometry3f _camera_offset;
    srrg_matchable::Scene* _scene_fixed;
    srrg_matchable::Scene* _scene_moving;
    srrg_matchable::Scene* _matchable_store;

    Isometry3fVector _camera_poses;
    int _to_draw_cameras;
    float _current_time;
    
    bool _play;
    bool _forward;
    bool _follow_camera;
    bool _save_snapshot;
    cv::Mat _descriptor;


    QBrush background;
    QBrush circleBrush;
    QFont textFont;
    QPen circlePen;
    QPen textPen;
  };

}
