#pragma once

#include <srrg_matchable/matchable.h>
#include <srrg_matchable/scene.h>
#include <srrg_sasha/base_registration_solver.h>
#include <srrg_sasha/iterative_registration_solver.h>
#include <srrg_matchable_detector/matchable_detector.h>

#include <srrg_sasha/data_association.h>
#include <srrg_sasha/nn_correspondence_finder.h>
#include <srrg_sasha/shapes_aligner.h>
#include <srrg_system_utils/colors.h>

#include <srrg_gl_helpers/opengl_primitives.h>

#include <qapplication.h>
#include <qglviewer.h>
#include <QKeyEvent>


using namespace srrg_gl_helpers;

#if QT_VERSION >= 0x050000
typedef qreal qglviewer_real;
#else
typedef float qglviewer_real;
#endif

namespace srrg_sasha{

  class StandardCamera: public qglviewer::Camera {
  protected:

    virtual qglviewer_real zFar() const { return 100; }
    virtual qglviewer_real zNear() const { return 0.1; }
  };

  class ShapesViewer : public QGLViewer {
  public:
    enum AssType{BruteForce=0x0, NNKdTree=0x1, Self=0x2, None=0x3};

    ShapesViewer() {
      StandardCamera * cam =new StandardCamera();
      setCamera(cam);
      cam->setPosition(qglviewer::Vec(5,5,5));
      cam->setViewDirection(qglviewer::Vec(-1,-1,-1));
    }

    void init() {
      restoreStateFromFile();
      s_fixed=new srrg_matchable::Scene();
      s_moving=new srrg_matchable::Scene();
      s_merged=new srrg_matchable::Scene();
      s_association = _ass.associations();
      nn_association = _nn_finder.associations();
      _ass_type = None;
      _solver = new srrg_sasha::IterativeRegistrationSolver();
      _iso.setIdentity();
      QColor white = QColor(Qt::white);
      setBackgroundColor(white);
      _shapes_aligner.setMaxIterations(10);
    }

    void addMatchableFixed(srrg_matchable::MatchablePtr matchable);
    void addMatchableMoving(srrg_matchable::MatchablePtr matchable);

    void addSceneFixed(const srrg_matchable::Scene& s);
    void addSceneMoving(const srrg_matchable::Scene& s);

    void clearScene();

    void selfCopy();
    void selfAssociate();

  protected :
    virtual void draw() {
      s_fixed->draw(false, Eigen::Vector3f(0.f, 0.f, 1.f));
      s_moving->draw(false, Eigen::Vector3f(0.f, 1.f, 0.f));
      s_association->draw();
      nn_association->draw(false, Eigen::Vector3f(1.f, 0.f, 0.f));
      s_merged->draw();
    }

    virtual void  keyPressEvent(QKeyEvent *event);

    void nnAssociate();
    void associate();
    void transform(const Eigen::Isometry3f& iso);
    void oneRound();
    void align();
    void merge();

    bool _association_done = false;

    srrg_matchable::Scene* s_fixed;
    srrg_matchable::Scene* s_moving;
    srrg_matchable::Scene* s_association;
    srrg_matchable::Scene* nn_association;
    srrg_matchable::Scene* s_merged;
    
    srrg_sasha::BaseRegistrationSolver* _solver;

    DataAssociation _ass;
    NNCorrespondenceFinder _nn_finder;
    ShapesAligner _shapes_aligner;

    AssType _ass_type;
    
    Eigen::Isometry3f _iso;
    float sign = 1.f;
  };

}
