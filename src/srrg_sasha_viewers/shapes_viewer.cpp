#include "shapes_viewer.h"

using namespace srrg_matchable;
using namespace srrg_sasha;

void ShapesViewer::addMatchableFixed(MatchablePtr matchable){
  s_fixed->push_back(matchable);
}

void ShapesViewer::addMatchableMoving(MatchablePtr matchable){
  s_moving->push_back(matchable);
}

void ShapesViewer::addSceneFixed(const Scene &s){
  std::cerr << "irrumatio" << std::endl;
  for(int i=0; i < s.size(); ++i){
    s[i]->mapId() = 0;
    s_fixed->push_back(s[i]);
  }
  std::cerr << "mintione" << std::endl;
}

void ShapesViewer::addSceneMoving(const Scene &s){
  std::cerr << "donkey punch" << std::endl;
  for(int i=0; i < s.size(); ++i) {
    s[i]->age() = 16;
    s_moving->push_back(s[i]);
  }
  std::cerr << "dirty sanchez" << std::endl;
}

void ShapesViewer::clearScene(){
  s_fixed->clear();
  s_moving->clear();
  // the clear has been overloaded in Scene, it deletes all the Matchable
}

void ShapesViewer::transform(const Eigen::Isometry3f& iso) {
  for(MatchablePtr m : *s_moving){
    m->transformInPlace(iso);
  }

}


void ShapesViewer::merge(){
  if(_ass_type != NNKdTree){
    std::cerr << FRED("[VIEWER][merge]: switch to NN-KD tree (N) to use merge!") << std::endl;
    return;
  }

  // int added = 0, merged = 0, cross_ass = 0;

  // for(MatchablePtr moving: *s_moving) {
  //     MatchablePtr fixed_associated = moving->association();
  //     if(!fixed_associated){
  //         s_merged->push_back(moving->clone());
  //         added++;
  //         continue;
  //       }
  //     if(moving->type() != fixed_associated->type()){
  //         //delete moving;
  //         cross_ass++;
  //         continue;
  //       }

  //     const Eigen::Vector3f& mp = moving->point();
  //     const Eigen::Vector3f& md = moving->directionVector();
  //     const Eigen::Vector3f& fp = fixed_associated->point();
  //     const Eigen::Vector3f& fd = fixed_associated->directionVector();

  //     Eigen::Vector3f p = (mp+fp)/2;
  //     Eigen::Vector3f d = (md+fd)/2;
  //     if(moving->type() == Matchable::Point)
  //       s_merged->push_back(new Matchable(Matchable::Point, p));
  //     else if(moving->type() == Matchable::Line)
  //       s_merged->push_back(new Matchable(Matchable::Line, p, d, moving->size()));

  //     merged++;

  // }

  // std::cerr << FGRN("[VIEWER][merge] merged: ") << merged << FGRN("  added: ") << added << FGRN("  cross-ass: ") << cross_ass << std::endl;

}

void ShapesViewer::associate(){
  _solver->constraints().clear();
  //setConstraints();
  s_association->clear();
  _ass.associate(DataAssociation::BruteForce, _solver->constraints(),s_fixed,s_moving);
  _association_done = true;
}

void ShapesViewer::nnAssociate(){
  _solver->constraints().clear();
  nn_association->clear();
  _nn_finder.setScenes(s_fixed, s_moving);
  _nn_finder.init();
  _nn_finder.compute(_solver->constraints());
  _association_done = true;
}

void ShapesViewer::oneRound() {
  if(!_association_done)
    std::cerr << "associate first! irrumatio" << std::endl;

  _solver->init();
  _solver->oneRound();

  std::cerr << "T: " << srrg_core::t2v(_solver->transform()).transpose() << std::endl;
  transform(_solver->transform());

  s_association->clear();
  nn_association->clear();
  _association_done = false;
}

void ShapesViewer::align(){

  _shapes_aligner.setT(Eigen::Isometry3f::Identity());

  _shapes_aligner.setFixedScene(s_fixed);
  _shapes_aligner.setMovingScene(s_moving);

  s_association->clear();
  nn_association->clear();

  _shapes_aligner.compute();
  transform(_shapes_aligner.T());
}

void ShapesViewer::selfCopy() {
  s_moving->clear();
  for(int i=0; i<s_fixed->size(); ++i){
    MatchablePtr fixed(new Matchable(*(s_fixed->at(i))));
    fixed->mapId() = -1;
    fixed->age() = 16;

    //merda
    const Eigen::Matrix3f& R = fixed->rotationMatrix();
    const Eigen::Vector3f x_offset = R.col(0)*0.25;
    const Eigen::Vector3f y_offset = R.col(1)*0.25;
    fixed->setPoint(fixed->point() + x_offset + y_offset);
    
    s_moving->push_back(fixed);
  }
}

void ShapesViewer::selfAssociate(){
  _solver->constraints().clear();
  for(int i=0; i<s_fixed->size(); ++i){
    _solver->constraints().push_back(BaseRegistrationSolver::Constraint(s_fixed->at(i),s_moving->at(i)));
  }
  _association_done = true;
}

void ShapesViewer::keyPressEvent(QKeyEvent *event) {
  Eigen::Isometry3f iso;
  srrg_core::Vector6f v;
  iso.setIdentity();
  bool aligner_verbosity = _shapes_aligner.verbosity();

  switch(event->key()){
  case Qt::Key_Plus:
    sign = 1.f;
    break;
  case Qt::Key_Minus:
    sign = -1.f;
    break;

  case Qt::Key_1:
    v << 0.1, 0, 0, 0, 0, 0;
    iso = srrg_core::v2t((srrg_core::Vector6f)(v*sign));
    _iso = _iso*iso;
    transform(iso);
    break;

  case Qt::Key_2:
    v << 0, 0.1, 0, 0, 0, 0;
    iso = srrg_core::v2t((srrg_core::Vector6f)(v*sign));
    _iso = _iso*iso;
    transform(iso);
    break;

  case Qt::Key_3:
    v << 0, 0, 0.1, 0, 0, 0;
    iso = srrg_core::v2t((srrg_core::Vector6f)(v*sign));
    _iso = _iso*iso;
    transform(iso);
    break;

  case Qt::Key_4:
    v << 0, 0, 0, 0.1, 0, 0;
    iso = srrg_core::v2t((srrg_core::Vector6f)(v*sign));
    _iso = _iso*iso;
    transform(iso);
    break;

  case Qt::Key_5:
    v << 0, 0, 0, 0, 0.1, 0;
    iso = srrg_core::v2t((srrg_core::Vector6f)(v*sign));
    _iso = _iso*iso;
    transform(iso);
    break;

  case Qt::Key_6:
    v << 0, 0, 0, 0, 0, 0.1;
    iso = srrg_core::v2t((srrg_core::Vector6f)(v*sign));
    _iso = _iso*iso;
    transform(iso);
    break;

  case Qt::Key_T:
    std::cerr << KCYN << "[VIEWER][KeyPress] T: " << srrg_core::t2v(_iso).transpose() << RST << std::endl;
    break;

  case Qt::Key_V:
    aligner_verbosity ^= 1;
    _shapes_aligner.setVerbosity(aligner_verbosity);
    _nn_finder.setVerbosity(aligner_verbosity);
    std::cerr << FCYN("[VIEWER][KeyPress] Changed Aligner Verbosity to: ") << aligner_verbosity<< std::endl;
    break;

  case Qt::Key_B:
    std::cerr << FCYN("[VIEWER][KeyPress] BF Associate") << std::endl;
    _ass_type = BruteForce;
    associate();
    break;

  case Qt::Key_N:
    std::cerr << FCYN("[VIEWER][KeyPress] NN Associate") << std::endl;
    _ass_type = NNKdTree;
    nnAssociate();
    break;

  case Qt::Key_M:
    std::cerr << FCYN("[VIEWER][KeyPress] Merging") << std::endl;
    s_merged->clear();
    merge();
    break;

  case Qt::Key_Space:
    std::cerr << FCYN("[VIEWER][KeyPress] One Round using ");
    if(_ass_type == None) {
      std::cerr << BOLD(FRED("stocazzo. Choose A/N")) << std::endl;
      return;
    } else if(_ass_type == BruteForce){
      std::cerr << FYEL("Brute Force") << std::endl;
      associate();
    } else if(_ass_type == NNKdTree){
      std::cerr << FYEL("NN KD-Tree") << std::endl;
      nnAssociate();
    } else if(_ass_type == Self){
      std::cerr << FYEL("Self :(") << std::endl;
      selfAssociate();
    }
    oneRound();
    break;

  case Qt::Key_S:
    std::cerr << FCYN("[VIEWER][KeyPress] Self Associate") << std::endl;
    _ass_type = Self;
    selfCopy();
    selfAssociate();
    break;

  case Qt::Key_Enter:
    std::cerr << FCYN("[VIEWER][KeyPress] Align (a.k.a risucchio)") << std::endl;
    align();
    break;

  default: QGLViewer::keyPressEvent(event);
  }
  updateGL();
}

