#pragma once

#include "point_detector.h"
#include "line_detector.h"
#include "plane_detector.h"
#include "surfel_detector.h"

namespace srrg_matchable_detector{

  class MatchableDetector{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    struct Config{
      float min_distance = 0.05f;
      float max_distance = 5.0f;
      float max_curvature = 1e-2f;      
      int col_gap = 5;
      int row_gap = 5;
      int normals_blur = 3;
      bool detect_points  = true;
      bool detect_lines   = true;
      bool detect_planes  = true;
      bool detect_surfels = true;
    };
    struct TimeStats{
      double detect = -1.f;
      double point_detection = -1.f;
      double line_detection = -1.f;
      double plane_detection = -1.f;
      double surfel_detection = -1.f;
      void print() const;
      std::string write() const;
    };
    
    MatchableDetector();

    Config& mutableConfig(){return *_configuration;}
    const TimeStats& timeStats() const {return _time_stats;}

    PointDetector& pointDetector() {return _point_detector;}
    LineDetector& lineDetector() {return _line_detector;}
    PlaneDetector& planeDetector() {return _plane_detector;}
    SurfelDetector& surfelDetector() {return _surfel_detector;}
    
    inline void setRowsAndCols(const int rows_, const int cols_){_rows=rows_; _cols=cols_;}
    inline void setK(const Eigen::Matrix3f& K_){_K = K_;}
    void init();
    void setImages(const srrg_core::RawDepthImage& raw_depth_image_, const srrg_core::RGBImage& rgb_image_ = srrg_core::RGBImage());
    void compute(srrg_matchable::Scene& matchables_);

    inline const srrg_core::Float3Image& normalsImage() const {return _normals_image;}
    inline const srrg_core::IntImage& regionsImage() const {return _regions_image;}

    inline const srrg_core::RGBImage& detectionsImage() const {return _detections_image;}

    void setShowDetections(bool show_detections_){_show_detections = show_detections_;}

    const std::vector<double>& timings() const {return _timings;}

  protected:
    void computeRegions();

    void drawDetections(srrg_matchable::Scene& matchables_);
    
  private:
    Config* _configuration;
    TimeStats _time_stats;
    bool _initialized;
    int _rows;
    int _cols;
    Eigen::Matrix3f _K;
    srrg_core::Float3Image _directions_image;
    srrg_core::FloatImage _depth_image;
    srrg_core::Float3Image _normals_image;
    srrg_core::Float3Image _cross_normals_image;
    srrg_core::Float3Image _points_image;
    srrg_core::FloatImage _curvature_image;
    srrg_core::IntImage _regions_image;
    srrg_core::RGBImage _rgb_image;
    srrg_core::UnsignedCharImage _gray_image;
    srrg_core::UnsignedCharImage _mask;

    srrg_core::RGBImage _detections_image;

    PointDetector _point_detector;
    LineDetector _line_detector;
    PlaneDetector _plane_detector;
    SurfelDetector _surfel_detector;

    bool _show_detections;

    std::vector<double> _timings;
  };
}
