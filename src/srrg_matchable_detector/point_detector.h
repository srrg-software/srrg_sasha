#pragma once
#include <srrg_matchable/matchable.h>
#include <srrg_matchable/scene.h>

#include <opencv2/core/version.hpp>
#include <opencv2/opencv.hpp>

#include <opencv/cv.h>
#include <opencv2/calib3d.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/xfeatures2d.hpp>

#include <srrg_system_utils/colors.h>

#include <Eigen/Geometry>

namespace srrg_matchable_detector {

  class PointDetector{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    struct Config{
      int threshold = 40;
      bool nonmaxSuppression = true;
    };

    PointDetector();
    ~PointDetector();
    
    Config& mutableConfig(){return _configuration;}

    void init();
    
    void  compute(srrg_matchable::Scene& matchables_,
                  const srrg_core::RGBImage& rgb_image_,
                  const srrg_core::Float3Image& points_image_,
                  const srrg_core::Float3Image& normals_image_);

  private:
    Config _configuration;
    bool _initialized;

    cv::Ptr<cv::FeatureDetector> _detector;
    cv::Ptr<cv::DescriptorExtractor> _descriptor_extractor;
  };
  

}
