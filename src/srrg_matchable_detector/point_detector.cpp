#include "point_detector.h"
#include <iostream>


namespace srrg_matchable_detector {

  using namespace srrg_matchable;

  PointDetector::PointDetector() {
    _initialized = false;
  }

  PointDetector::~PointDetector() {
    delete _detector;
  }


  void PointDetector::init() {
    std::cerr << FGRN("[PointDetector] init:") << std::endl;
    std::cerr << "Configuration: " << std::endl;
    std::cerr << "\t>>threshold: " << _configuration.threshold << std::endl;
    std::cerr << "\t>>nonmaxSuppression: " << _configuration.nonmaxSuppression << std::endl;

    _detector = cv::FastFeatureDetector::create(_configuration.threshold,
                                                _configuration.nonmaxSuppression);
    
    _descriptor_extractor = cv::xfeatures2d::BriefDescriptorExtractor::create();
    
    
    _initialized = true;
    
  }

  void PointDetector::compute(Scene &matchables_,
                              const srrg_core::RGBImage& rgb_image_,
                              const srrg_core::Float3Image& points_image_,
                              const srrg_core::Float3Image& normals_image_) {
    if(!_initialized)
      throw std::runtime_error("[PointDetector][compute]: not initialized");
    
    // keypoint buffer
    std::vector<cv::KeyPoint> keypoints;
    cv::Mat descriptors;
    // detect keypoints
    _detector->detect(rgb_image_, keypoints);
    _descriptor_extractor->compute(rgb_image_, keypoints, descriptors);
    
    const int keypoints_size = keypoints.size();
    if(!keypoints_size){
      std::cerr << "warning: [PointDetector] found 0 keypoints!\n";
      return;
    }

    // Get 3d points and generate matchables
    for(size_t i = 0; i < keypoints_size; ++i) {
      const cv::Point2f& keypoint = keypoints[i].pt;
      const cv::Vec3f point = points_image_.at<cv::Vec3f>(keypoint.y, keypoint.x);
      const cv::Vec3f normal = normals_image_.at<cv::Vec3f>(keypoint.y, keypoint.x);
      const float depth = cv::norm(point);
      if(depth < 1e-2 || cv::norm(normal) < 1e-2)
        continue;
      const Eigen::Vector3f p(point(0), point(1), point(2));
      const Eigen::Vector3f n(normal(0), normal(1), normal(2));

      // create shared and add
      MatchablePtr point_matchable(new Matchable(Matchable::Point,p));
      //MatchablePtr point_matchable(new Matchable(Matchable::Surfel,p,n));

      point_matchable->setDescriptor(descriptors.row(i));
      matchables_.push_back(point_matchable);
    }
  }
}
