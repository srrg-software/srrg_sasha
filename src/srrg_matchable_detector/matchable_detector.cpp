#include "matchable_detector.h"

using namespace std;
using namespace srrg_core;
using namespace srrg_matchable;

namespace srrg_matchable_detector{

  void MatchableDetector::TimeStats::print() const {
    std::cerr << FYEL("Matchable Detector Time Stats") << std::endl;
    std::cerr << "detection:         " << detect << std::endl;
    std::cerr << "|" << std::endl;
    std::cerr << "|---> point:       " << point_detection << std::endl;
    std::cerr << "|---> line:        " << line_detection << std::endl;
    std::cerr << "|---> plane:       " << plane_detection << std::endl;
    std::cerr << "|---> surfel:      " << surfel_detection << std::endl;
  }

  std::string MatchableDetector::TimeStats::write() const {
    std::stringstream stats;
    stats << "Matchable Detector Time Stats" << std::endl;
    stats << "detection:         " << detect << std::endl;
    stats << "|" << std::endl;
    stats << "|---> point:       " << point_detection << std::endl;
    stats << "|---> line:        " << line_detection << std::endl;
    stats << "|---> plane:       " << plane_detection << std::endl;
    stats << "|---> surfel:      " << surfel_detection << std::endl;

    return stats.str();
  }

  MatchableDetector::MatchableDetector(){
    _rows=0;
    _cols=0;
    _K = Eigen::Matrix3f::Zero();
    _initialized = false;
    _configuration = new Config;
    _show_detections = false;
  }

  void MatchableDetector::init(){
    assert(_rows != 0 && "rows != 0");
    assert(_cols != 0 && "cols != 0");
    assert(_K != Eigen::Matrix3f::Zero() && "K != 0");

    _directions_image.create(_rows,_cols);
    initializePinholeDirections(_directions_image,_K);

    _points_image.create(_rows,_cols);
    _normals_image.create(_rows,_cols);
    _curvature_image.create(_rows,_cols);
    _regions_image.create(_rows, _cols);
    
    _point_detector.init();
    _line_detector.init();
    _plane_detector.init();
    _surfel_detector.init();
    
    _initialized = true;
  }


  void MatchableDetector::computeRegions(){

    _regions_image = PlaneDetector::PixelType::Line;
    for(int r=1; r<_rows; ++r){
        const float* c_ptr = _curvature_image.ptr<const float>(r)+1;
        int* regions_ptr = _regions_image.ptr<int>(r)+1;
        for(int c=1; c<_cols-1; ++c, ++c_ptr,++regions_ptr){
          if(*c_ptr < _configuration->max_curvature)
              *regions_ptr = PlaneDetector::PixelType::Surfel; //init as surfel
          }
      }
  }


  // TODO avoid useless computation when not needed
  void MatchableDetector::setImages(const RawDepthImage &raw_depth_image_,
                                    const RGBImage &rgb_image_){
    assert(_initialized && "not initialized");
    convert_16UC1_to_32FC1(_depth_image, raw_depth_image_);

    //compute normal_image and points_image
    computePointsImage(_points_image,
                       _directions_image,
                       _depth_image,
                       _configuration->min_distance,
                       _configuration->max_distance);

    computeSimpleNormals(_cross_normals_image,
                         _points_image,
                         _configuration->col_gap,
                         _configuration->row_gap,
                         _configuration->max_distance);

    normalBlur(_normals_image,_cross_normals_image,_configuration->normals_blur);

//    _normals_image = _cross_normals_image;

    computeCurvature(_curvature_image,_normals_image);
    computeRegions();
  
    _rgb_image = rgb_image_;
    cv::cvtColor(_rgb_image,_gray_image,CV_BGR2GRAY);
  }

  void MatchableDetector::compute(Scene& matchables_){
    _time_stats.detect = srrg_core::getTime();
    
    // Point Detection
    if(_configuration->detect_points) {
      _time_stats.point_detection = srrg_core::getTime();
      _point_detector.compute(matchables_,
                              _rgb_image,
                              _points_image,
                              _normals_image);
      _time_stats.point_detection = srrg_core::getTime() - _time_stats.point_detection;
    }

    // Line Detection
    if(_configuration->detect_lines) {
      _time_stats.line_detection = srrg_core::getTime();
      _line_detector.compute(matchables_,
                             _gray_image,
                             _points_image,
                             _curvature_image);
      _time_stats.line_detection = srrg_core::getTime() - _time_stats.line_detection;
    }

    // Plane Detection
    if(_configuration->detect_planes) {
      _time_stats.plane_detection = srrg_core::getTime();
      _plane_detector.compute(matchables_,
                              _regions_image,
                              _points_image,
                              _normals_image,
                              _curvature_image);
      _time_stats.plane_detection = srrg_core::getTime() - _time_stats.plane_detection;
    }

    // Surfel Detection
    if(_configuration->detect_surfels) {
      _time_stats.surfel_detection = srrg_core::getTime();
      _surfel_detector.compute(matchables_,
                               _points_image,
                               _normals_image,
                               _regions_image);
      _time_stats.surfel_detection = srrg_core::getTime() - _time_stats.surfel_detection;
    }

    _time_stats.detect = srrg_core::getTime() - _time_stats.detect;

    _timings.push_back(_time_stats.detect);

    if(_show_detections){
        drawDetections(matchables_);
    }
  }


  void showPoint(RGBImage& detection_image,
                 MatchablePtr m,
                 const Eigen::Matrix3f& K) {

    const Eigen::Vector3f& p = m->point();
    Eigen::Vector3f coord = K*p;
    coord /= coord(2);

    cv::circle(detection_image, cv::Point2f(coord.x(), coord.y()), 2, cv::Scalar( 0, 0, 255 ),2);

  }

  void showSurfel(RGBImage& detection_image,
                  MatchablePtr m,
                  const Eigen::Matrix3f& K) {

    const Eigen::Vector3f& p = m->point();
    Eigen::Vector3f coord = K*p;
    coord /= coord(2);

    cv::circle(detection_image, cv::Point2f(coord.x(), coord.y()), 2, cv::Scalar(  255, 0, 255 ),2);

  }

  void showLine(RGBImage& detection_image,
                MatchablePtr m,
                const Eigen::Matrix3f& K) {

    // project
    const Eigen::Vector3f& p = m->point();
    const Eigen::Vector3f& d = m->directionVector();
    const Eigen::Vector2f& e = m->extent();

    Eigen::Vector3f coordA = K*p;
    coordA /= coordA(2);

    Eigen::Vector3f q = p + d*e.x();
    Eigen::Vector3f coordB = K*q;
    coordB /= coordB(2);

    cv::line(detection_image,
             cv::Point (coordA.x(), coordA.y()),
             cv::Point (coordB.x(), coordB.y()),
             cv::Scalar (0,255,0),2);
  }

  void showPlane(RGBImage& detection_image,
                 MatchablePtr m,
                 const Eigen::Matrix3f& K) {

    Eigen::Isometry3f transform;
    transform.setIdentity();
    transform.linear() = m->rotationMatrix();
    transform.translation() = m->point();

    // project
    const Cloud3D& cloud = m->cloud();
    std::vector<cv::Point> points;

    for(size_t i=0;i<cloud.size();++i){

      Eigen::Vector3f coord = K*transform*cloud[i].point();
      coord /= coord(2);
      points.push_back(cv::Point(coord.x(),coord.y()));
      cv::circle(detection_image,cv::Point2f(coord.x(),coord.y()),2,cv::Scalar(255,0,0),2);
    }
  }

  void MatchableDetector::drawDetections(srrg_matchable::Scene& matchables_){

//      _rgb_image.copyTo(_detections_image);

      RGBImage detection_image;
      _rgb_image.copyTo(detection_image);


      for(size_t i = 0; i < matchables_.size(); ++i){

        MatchablePtr m = matchables_[i];
        const Matchable::Type type = m->type();

        switch(type) {
        case Matchable::Point:
          showPoint(detection_image, m, _K);
          break;
        case Matchable::Line:
          showLine(detection_image, m, _K);
          break;
        case Matchable::Plane:
          showPlane(detection_image, m, _K);
          break;
        case Matchable::Surfel:
          showSurfel(detection_image, m, _K);
          break;
        default:
          break;
        }
      }

      addWeighted( _rgb_image, 0.5, detection_image, 0.5, 0.0, _detections_image);

  }
}
