#include "direct_registration_solver.h"
#include <Eigen/Cholesky>
#include <Eigen/SVD>

namespace srrg_sasha{

  using namespace srrg_matchable;
  
  DirectRegistrationSolver::Vector12f DirectRegistrationSolver::flattenIsometry(const DirectRegistrationSolver::Isometry3f& iso){
    Vector12f v=Vector12f::Zero();
    v.block<3,1>(0,0)=iso.translation();
    v.block<3,1>(3,0)=iso.linear().row(0).transpose();
    v.block<3,1>(6,0)=iso.linear().row(1).transpose();
    v.block<3,1>(9,0)=iso.linear().row(2).transpose();
    return v;
  }
  
  DirectRegistrationSolver::Isometry3f DirectRegistrationSolver::unflattenIsometry(const DirectRegistrationSolver::Vector12f& v) {
    Isometry3f iso=Isometry3f::Identity();
    iso.translation()=v.block<3,1>(0,0);
    iso.linear().row(0)=v.block<3,1>(3,0).transpose();
    iso.linear().row(1)=v.block<3,1>(6,0).transpose();
    iso.linear().row(2)=v.block<3,1>(9,0).transpose();
    return iso;
  }

  DirectRegistrationSolver::Matrix6_12f
  DirectRegistrationSolver::jacobianT(const Eigen::Vector3f& point,
                                      const Eigen::Vector3f& direction){
    Matrix6_12f Jt=Matrix6_12f::Zero();
    Jt.block<3,3>(0,0).setIdentity();
    Jt.block<1,3>(0,3)=point.transpose();
    Jt.block<1,3>(1,6)=point.transpose();
    Jt.block<1,3>(2,9)=point.transpose();
    Jt.block<1,3>(3,3)=direction.transpose();
    Jt.block<1,3>(4,6)=direction.transpose();
    Jt.block<1,3>(5,9)=direction.transpose();
    return Jt;
  }

  DirectRegistrationSolver::DirectRegistrationSolver() {
    _H.setZero();
    _b.setZero();
    std::cerr <<"drs constriucted" << std::endl;
  }

  DirectRegistrationSolver::~DirectRegistrationSolver() { }

  // computes the error and the Jacobian of a constraint
  void DirectRegistrationSolver::errorAndJacobian(Vector3f& e_p,    // point-error
                                                  Vector3f& e_d,    // direction-error
                                                  float& e_o,              // orthogonality error
                                                  float& e_x,              // intersection error
                                                  Matrix3_12f& J_p,  // point jacobian
                                                  Matrix3_12f& J_d,  // direction jacobian
                                                  Matrix1_12f& J_o,  // orthogonality jacobian
                                                  Matrix1_12f& J_x,  // orthogonality error
                                                  bool compute_p,          // if true computes the point part
                                                  bool compute_d,          // if true computes the direction part
                                                  bool compute_o,          // if true computes the orthogonality part
                                                  bool compute_x,          // if true computes the orthogonality part
                                                  const MatchablePtr fixed,  // input fixed item
                                                  const MatchablePtr moving // input moving item
                                                  )
  {

    Matrix6_12f J_t=jacobianT(moving->point(), moving->rotation().col(0));
    
    // transform the moving item and put direction point and direction vectors
    const Eigen::Vector3f p_moving = _T*moving->point();
    const Eigen::Vector3f d_moving = _T.linear()*moving->rotation().col(0);
    const Eigen::Vector3f& p_fixed = fixed->point();
    const Eigen::Matrix3f& R_fixed = fixed->rotation();
    const Eigen::Vector3f& d_fixed = fixed->rotation().col(0);
    const Eigen::Matrix3f R_fixed_prime = R_fixed.transpose();

    // compute the always useful cross produce terms
    Matrix3f p_moving_cross=srrg_core::skew((const Eigen::Vector3f&)p_moving);
    Matrix3f d_moving_cross=srrg_core::skew((const Eigen::Vector3f&)d_moving);

    Matrix3_9f J_p_t = Matrix3_9f::Zero();
    J_p_t.block<1,3>(0,0)=moving->point().transpose();
    J_p_t.block<1,3>(1,3)=moving->point().transpose();
    J_p_t.block<1,3>(2,6)=moving->point().transpose();

    Matrix3_9f J_d_t = Matrix3_9f::Zero();
    J_d_t.block<1,3>(0,0)=moving->rotation().col(0).transpose();
    J_d_t.block<1,3>(1,3)=moving->rotation().col(0).transpose();
    J_d_t.block<1,3>(2,6)=moving->rotation().col(0).transpose();

    e_p=R_fixed_prime*( p_moving-p_fixed);
    
    if (compute_p){
      // point error and jacobian

      Matrix3_6f J_pp=Matrix3_6f::Zero();
//      J_pp.block<3,3>(0,0).setIdentity();
//      J_p=J_pp*J_t;
      J_p.setZero();
      J_p.block<3,3>(0,0)=R_fixed_prime;
      J_p.block<3,9>(0,3)=R_fixed_prime*J_p_t;
    }
    // direction error and jacobian
    if (compute_d) {
      e_d=d_moving-d_fixed;
//      Matrix3_6f J_dd=Matrix3_6f::Zero();
//      J_dd.block<3,3>(0,3).setIdentity();
//      J_d=J_dd*J_t;
      J_d.setZero();
      J_d.block<3,3>(0,0).setZero();
      J_d.block<3,9>(0,3)=J_d_t;
    }
    // orthogonality error and jacobian
    if(compute_o){
      e_o=d_moving.dot(d_fixed);
//      Matrix1_6f J_oo=Matrix1_6f::Zero();
//      J_oo.block<1,3>(0,3)=d_fixed.transpose();
//      J_o=J_oo*J_t;
      J_o.setZero();
      J_o.block<1,3>(0,0).setZero();
      J_o.block<1,9>(0,3)=d_fixed.transpose()*J_d_t;
    }
    // intersection error and jacobian
    if(compute_x){
      Vector3f d_moving_cross_d_fixed=d_moving.cross(d_fixed);
      e_x=e_p.dot(d_moving_cross_d_fixed);
      Matrix1_6f J_xx = Matrix1_6f::Zero();
      J_xx.block<1,3>(0,0)=d_moving_cross_d_fixed.transpose();
      J_xx.block<1,3>(0,3)=-e_p.transpose()* srrg_core::skew((Eigen::Vector3f)d_fixed);
      J_x=J_xx*J_t;
    }
  }

  

  void DirectRegistrationSolver::init(const Eigen::Isometry3f& transform_,
                                      const bool use_support_weight) {
    BaseRegistrationSolver::init(transform_, use_support_weight);
    _H.setZero();
    _b.setZero();
  }


  // generic constraint linearization
  // side effect on _H and _b
  void DirectRegistrationSolver::linearizeConstraint(Constraint& constraint)
  {

    using namespace std;
    const MatchablePtr moving=constraint.moving;
    const MatchablePtr fixed=constraint.fixed;

    Eigen::Matrix3f fixed_weight = Eigen::Matrix3f::Zero(), moving_weight  = Eigen::Matrix3f::Zero(), constraint_weight  = Eigen::Matrix3f::Zero();
    moving->weight(moving_weight);
    fixed->weight(fixed_weight);
    constraint_weight = (moving_weight.determinant() > fixed_weight.determinant())?  moving_weight : fixed_weight; 
    
    // buffer variables to avoid abusing the stack
    Vector3f e_p = Vector3f::Zero();
    Vector3f e_d = Vector3f::Zero();
    float e_o = 0.f;
    float e_x = 0.f;
    Matrix3_12f J_p = Matrix3_12f::Zero();
    Matrix3_12f J_d = Matrix3_12f::Zero();
    Matrix1_12f J_o = Matrix1_12f::Zero();
    Matrix1_12f J_x = Matrix1_12f::Zero();


    // the following variable will contain
    // what we have to do to linearize this constraint
    SelectionMatrixEntry entry;

    // we handle differently the intersection constraint,
    // that is not contained in the action matrix
    if (constraint.is_intersection){
      assert(moving->type()==Matchable::Line&&
             moving->type()==Matchable::Line&&
             "DirectRegistrationSolver::linearizeConstraint(): " &&
             "for an intersection constraint" &&
             "the types should be both lines");
      entry.compute_p=false;
      entry.compute_d=false;
      entry.compute_o=false;
      entry.compute_x=true;
      entry.rotate_omega=false;
    } else {
      // fetch on the action matrix which parts of the jacobians
      // we have to compute
      // and how to deal with the omegas
      entry=_selection_matrix[moving->dof()][fixed->dof()];
    }
    
    /*

      cerr << "moving type: " << moving->type() << endl;
      cerr << "fixed type: " << fixed->type() << endl;

      cerr << "entry, selectin_matrix" << endl;
      cerr << "compute_p: "<< entry.compute_p << " ";
      cerr << "compute_d: "<< entry.compute_d << " ";
      cerr << "compute_o: "<< entry.compute_o << " ";
      cerr << "compute_x: "<< entry.compute_x << " ";
      cerr << "rotate:    "<< entry.rotate_omega << endl;;

    */

    //compute the jacobian

    errorAndJacobian(e_p, e_d, e_o, e_x,
                     J_p, J_d, J_o, J_x,
                     entry.compute_p,
                     entry.compute_d,
                     entry.compute_o,
                     entry.compute_x,
                     fixed,
                     moving);
    // handle point term
    constraint.resetErrors();
    // temporary variables to hold the updates
    Matrix12f H_temp=Matrix12f::Zero();
    Vector12f b_temp=Vector12f::Zero();
    bool is_inlier=true;
    if (entry.compute_p){
      Matrix3f omega_p=fixed->omega();

      // this is to handle the case when we have to rotate the omega matrix
      // during the iterations
//      if (!puttana && entry.rotate_omega) {
//        const Eigen::Matrix3f& R=_T.linear();
//        omega_p=R*moving->directionMatrix()*R.transpose();
//      }

      if(_use_support_weight){
        omega_p += constraint_weight;
      }

      constraint.error_p=e_p.transpose()*omega_p*e_p;
      if(constraint.error_p>_point_kernel_threshold) {
        float scale=sqrt(_point_kernel_threshold/constraint.error_p);
        omega_p*=scale;
        is_inlier=false;
      }
      H_temp+=J_p.transpose()*omega_p*J_p;
      b_temp+=J_p.transpose()*omega_p*e_p;
    }
    // handle the direction term
    if (entry.compute_d){
      Eigen::Matrix3f omega_d=_omega_d;
      if(_use_support_weight){
        omega_d += constraint_weight;
      }
      constraint.error_d=e_d.transpose()*omega_d*e_d;
      if(constraint.error_d>_direction_kernel_threshold) {
        float scale=sqrt(_direction_kernel_threshold/constraint.error_d);
        omega_d*=scale;
        is_inlier=false;
      }
      H_temp+=J_d.transpose()*omega_d*J_d;
      b_temp+=J_d.transpose()*omega_d*e_d;
    }
    // handle the orthogonality term
    if (entry.compute_o){
      float omega_o=_omega_o;
      if(_use_support_weight){
        omega_o += constraint_weight(0,0);
      }
      constraint.error_o=e_o*omega_o*e_o;
      if(constraint.error_o>_orthogonal_kernel_threshold) {
        float scale=sqrt(_orthogonal_kernel_threshold/constraint.error_o);
        omega_o*=scale;
        is_inlier=false;
      }
      H_temp+=J_o.transpose()*omega_o*J_o;
      b_temp+=J_o.transpose()*omega_o*e_o;
    }
    // handle the direction term
    if (entry.compute_x){
      constraint.error_x=e_x*_omega_x*e_x;
      float omega_x=_omega_x;
      if(constraint.error_x>_intersection_kernel_threshold) {
        float scale=sqrt(_intersection_kernel_threshold/constraint.error_x);
        omega_x*=scale;
        is_inlier=false;
      }
      H_temp+=J_x.transpose()*omega_x*J_x;
      b_temp+=J_x.transpose()*omega_x*e_x;
    }
    
    if (is_inlier || ! _ignore_outliers){     
      _H+=H_temp;
      _b+=b_temp;
    }
  }

  void DirectRegistrationSolver::oneRound(){
    if (_che_cazzo_devo_fare_upper)
      oneRoundUpper();
    else
      oneRoundLower();
  }
  
  void DirectRegistrationSolver::oneRoundUpper(){
    using namespace std;
    cerr << "Solver T (before)" << endl;
    cerr << _T.matrix() << endl;
    _H.setZero();
    _b.setZero();
    for (Constraint& constraint:_constraints) {
      linearizeConstraint(constraint);
    }
    _H+=Matrix12f::Identity()*_damping;
    Vector12f dx=_H.ldlt().solve(-_b);

    Vector12f x=flattenIsometry(_T);
    x+=dx;
    Eigen::Isometry3f temp_T=unflattenIsometry(x);
    Eigen::JacobiSVD<Eigen::Matrix3f> svd(temp_T.linear(), Eigen::ComputeThinU | Eigen::ComputeThinV);
    temp_T.linear()=svd.matrixU()*svd.matrixV().transpose();
    _T=temp_T;

    // round 2, lock the translation, and solve for the rotation
    _H.setZero();
    _b.setZero();
    for (Constraint& constraint:_constraints) {
      linearizeConstraint(constraint);
    }
    _H+=Matrix12f::Identity()*_damping;

    Matrix3f Ht=_H.block<3,3>(0,0);
    Vector3f bt=_b.block<3,1>(0,0);
    Vector3f dt=Ht.ldlt().solve(-bt);
    _T.translation()+=dt;
    // cerr << "Solver T (after)" << endl;
    // cerr << _T.matrix() << endl;
  }

  void DirectRegistrationSolver::oneRoundLower(){
    using namespace std;
    // cerr << "Solver T (before)" << endl;
    // cerr << _T.matrix() << endl;

    Eigen::Vector3f y= _T.linear().transpose()*_T.translation();
    Eigen::Isometry3f T2=_T;
    T2.translation()=y;
    
    _H.setZero();
    _b.setZero();
    for (Constraint& constraint:_constraints) {
      linearizeConstraint(constraint);
    }
    _H+=Matrix12f::Identity()*_damping;
    Vector12f dx=_H.ldlt().solve(-_b);

    Vector12f x=flattenIsometry(T2);
    x+=dx;
    Eigen::Isometry3f temp_T=unflattenIsometry(x);
    Eigen::JacobiSVD<Eigen::Matrix3f> svd(temp_T.linear(), Eigen::ComputeThinU | Eigen::ComputeThinV);
    temp_T.linear()=svd.matrixU()*svd.matrixV().transpose();

    _T=temp_T;
    _T.translation()=_T.linear()*_T.translation();



    // round 2, lock the translation, and solve for the rotation
    y= _T.linear().transpose()*_T.translation();
    T2=_T;
    T2.translation()=y;

    _H.setZero();
    _b.setZero();
    for (Constraint& constraint:_constraints) {
      linearizeConstraint(constraint);
    }
    _H+=Matrix12f::Identity()*_damping;

    Matrix3f Ht=_H.block<3,3>(0,0);
    Vector3f bt=_b.block<3,1>(0,0);
    Vector3f dt=Ht.ldlt().solve(-bt);
    T2.translation()+=dt;

    _T=T2;
    _T.translation()=_T.linear()*_T.translation();
    
    // cerr << "Solver T (after)" << endl;
    // cerr << _T.matrix() << endl;
  }

}
