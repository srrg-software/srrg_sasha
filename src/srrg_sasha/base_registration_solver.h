#pragma once
#include <srrg_matchable/matchable.h>
#include <iostream>

namespace srrg_sasha{
  class BaseRegistrationSolver {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    // This type represents a constraint between two matchables
    // all constraints are assumed to be of containment type
    // unless is_intersection is specified
    struct Constraint{
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
      Constraint(const srrg_matchable::MatchablePtr& fixed=nullptr,
                 const srrg_matchable::MatchablePtr& moving=nullptr,
                 bool is_intersection=false);
      void resetErrors();
      srrg_matchable::MatchablePtr fixed = nullptr;
      srrg_matchable::MatchablePtr moving = nullptr;
      int weight = 1; //< the omega will be multiplied by this weight
      bool is_intersection; //< true if the constraint is an intersection between lines
      float error_p; //< the solver writes here the point error at each round
      float error_d; //< the solver writes here the directon error at each round
      float error_o; //< the solver writes here the orthogonality error at each round
      float error_x; //< the solver writes here the intersection error at each round
    };

    typedef std::vector<Constraint, Eigen::aligned_allocator<Constraint> > ConstraintVector;

    // constructs an empty solver
    BaseRegistrationSolver();

    // destroys the solver
    virtual ~BaseRegistrationSolver();

    // prepares the optimization, starting from the specified pose
    virtual void init(const Eigen::Isometry3f& transform_=Eigen::Isometry3f::Identity(),
                      const bool use_support_weight = false);
    
    // executes one round of optimization
    virtual void oneRound()=0;

    // accessor to the vector of constraints
    inline const ConstraintVector& constraints() const { return _constraints; }
    inline ConstraintVector& constraints()  {return _constraints;}

    // the current transform
    inline const Eigen::Isometry3f& transform() const {return _T;}
    inline void setTransform(const Eigen::Isometry3f& transform_=Eigen::Isometry3f::Identity()) {
      _T=transform_;
    }


    // damping parameter. Higher damping slower but more conservative convergence
    inline float damping() const {return _damping;}
    inline void setDamping(float damping_) {_damping=damping_;}

    // sets the weight of the direction factor
    // higher values result in more importance given to plane normals
    // and line directions
    inline float omegaDirection() const {return _omega_d_scalar;}
    inline void  setOmegaDirection(float omega_d_scalar)  {
      _omega_d_scalar=omega_d_scalar;
      _omega_d=Eigen::Matrix3f::Identity()*_omega_d_scalar;
    }

    // sets the weight for the line-plane containment
    // higher value results in this class of constraints more satisfied than others
    inline float omegaOrthogonal() const {return _omega_o;}
    inline void  setOmegaOrthogonal(float omega_o)  { _omega_o = omega_o;}

    // weight for the intersection constraints
    inline float omegaIntersection() const {return _omega_x;}
    inline void  setOmegaIntersection(float omega_x)  { _omega_x = omega_x;}


    // threshold for point-point/ point-line / point-plane distances
    inline float pointKernelThreshold() const { return _point_kernel_threshold;}
    inline void setPointKernelThreshold(float threshold)  { _point_kernel_threshold=threshold;}

    // threshold for the direction vector of planes-plane and line-line distances
    inline float directionKernelThreshold() const { return _direction_kernel_threshold;}
    inline void setDirectionKernelThreshold(float threshold)  { _direction_kernel_threshold=threshold;}

    // threshold for line-plane direction orthogonality
    inline float orthogonalKernelThreshold() const { return _orthogonal_kernel_threshold;}
    inline void setOrthogonalKernelThreshold(float threshold)  { _orthogonal_kernel_threshold=threshold;}

    // threshold for line-line intersection
    inline float intersectionKernelThreshold() const { return _intersection_kernel_threshold;}
    inline void setIntersectionKernelThreshold(float threshold)  { _intersection_kernel_threshold=threshold;}

    // if set to true outliers (avove kernels) are ignored
    inline bool ignoreOutliers() const {return _ignore_outliers;}
    inline void setIgnoreOutliers(bool ignore_outliers)  { _ignore_outliers=ignore_outliers; }

    inline bool& cheCazzoDevoFare() {return _che_cazzo_devo_fare_upper;}

  protected:
    // handy typedefs
    typedef Eigen::Isometry3f Isometry3f;
    typedef Eigen::Matrix<float,3,6> Matrix3_6f;
    typedef Eigen::Matrix<float,3,9> Matrix3_9f;
    typedef Eigen::Matrix<float,1,6> Matrix1_6f;
    typedef Eigen::Vector3f Vector3f;
    typedef Eigen::Matrix3f Matrix3f;
    typedef Eigen::Matrix<float,6,6> Matrix6f;
    typedef Eigen::Matrix<float,6,1> Vector6f;
    


    ConstraintVector _constraints;         //< constraint vector
    bool _ignore_outliers;                 //< if true, outliers are ignored in optimization
    Eigen::Isometry3f _T;                  //< transform
    Matrix3f _omega_d;                     //< temp spherical omega for direction vectors
    float _omega_d_scalar;                 //< spherical direction omega magnutude
    float _omega_o;                        //< omega of orthogonality (line-plane)
    float _omega_x;                        //< omega of intersection
    float _damping;                        //< damping (for milder convergence)
    float _point_kernel_threshold;         //< threshold for point error
    float _direction_kernel_threshold;     //< threshold for direction error
    float _orthogonal_kernel_threshold;    //< threshold for orthogonality error (plane-line)
    float _intersection_kernel_threshold;  //< threshold for intersection
    bool _use_support_weight;              //< if true, use the contraint weights

    bool _che_cazzo_devo_fare_upper;
    
    //! entry of action matrix that tells what to do with a specific constraint
    struct SelectionMatrixEntry{
      bool compute_p;
      bool compute_d;
      bool compute_o;
      bool compute_x;
      bool rotate_omega;
      bool reverse;
    };

    static const SelectionMatrixEntry _selection_matrix[][srrg_matchable::Matchable::Dim];
  };
  
}
