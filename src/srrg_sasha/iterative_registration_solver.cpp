#include "iterative_registration_solver.h"
#include <Eigen/Cholesky>

namespace srrg_sasha{
  using namespace srrg_matchable;
  IterativeRegistrationSolver::IterativeRegistrationSolver() {
    _H.setZero();
    _b.setZero();

    _chi_square = 0;
    _num_inliers = 0;

    std::cerr <<"is constructed!" << std::endl;
  }

  IterativeRegistrationSolver::~IterativeRegistrationSolver() { }

  // computes the error and the Jacobian of a constraint
  void IterativeRegistrationSolver::errorAndJacobian(Vector3f& e_p,    // point-error
                                                     Vector3f& e_d,    // direction-error
                                                     float& e_o,              // orthogonality error
                                                     float& e_x,              // intersection error
                                                     Matrix3_6f& J_p,  // point jacobian
                                                     Matrix3_6f& J_d,  // direction jacobian
                                                     Matrix1_6f& J_o,  // orthogonality jacobian
                                                     Matrix1_6f& J_x,  // orthogonality error
                                                     bool compute_p,          // if true computes the point part
                                                     bool compute_d,          // if true computes the direction part
                                                     bool compute_o,          // if true computes the orthogonality part
                                                     bool compute_x,          // if true computes the orthogonality part
                                                     const MatchablePtr& fixed,  // input fixed item
                                                     const MatchablePtr& moving, // input moving item
                                                     const bool rotate_omega)
  {
    // transform the moving item and put direction point and direction vectors
    const Eigen::Vector3f p_moving = _T*moving->point();
    const Eigen::Matrix3f R_moving = _T.linear()*moving->rotation();
    const Eigen::Vector3f d_moving = _T.linear()*moving->rotation().col(0);
    const Eigen::Vector3f& p_fixed = fixed->point();
    const Eigen::Matrix3f& R_fixed = fixed->rotation();
    const Eigen::Vector3f& d_fixed = fixed->rotation().col(0);
    const Eigen::Matrix3f R_fixed_prime = R_fixed.transpose();
    const Eigen::Matrix3f R_moving_prime= moving->rotation().transpose();

    // std::cerr << "correspondence info: " << std::endl;
    // std::cerr << "p_mov: " << p_moving.transpose() << std::endl;    
    // std::cerr << "d_mov: " << d_moving.transpose() << std::endl;
    // std::cerr << "p_fix: " << p_fixed.transpose() << std::endl;
    // std::cerr << "d_fix: " << d_fixed.transpose() << std::endl;
    // std::cerr << "R_fixed:\n" << R_fixed << std::endl;
    // std::cerr << "R_moving:\n" << R_moving << std::endl;
    
    // compute the always useful cross produce terms
    Matrix3f p_moving_cross=srrg_core::skew((const Eigen::Vector3f&)p_moving);
    Matrix3f d_moving_cross=srrg_core::skew((const Eigen::Vector3f&)d_moving);
    Matrix3f d_fixed_cross =srrg_core::skew((const Eigen::Vector3f&)d_fixed);

    // point error and jacobian
    e_p=R_fixed_prime*(p_moving-p_fixed);
    
    if (compute_p){ 
      J_p.block<3,3>(0,0)=R_fixed_prime;
      J_p.block<3,3>(0,3)=-R_fixed_prime*p_moving_cross;
    }

    // direction error and jacobian
    if (compute_d) {
      e_d=d_moving-d_fixed;
      J_d.block<3,3>(0,0).setZero();
      J_d.block<3,3>(0,3)=-d_moving_cross;
    }

    // orthogonality error and jacobian
    if(compute_o){     
      e_o=d_moving.dot(d_fixed);
      J_o.setZero();
      J_o.block<1,3>(0,3)=(R_moving_prime*_T.linear().transpose()*d_fixed_cross).row(2);
    }

    // intersection error and jacobian
    if(compute_x){
      Eigen::Vector3f d_moving_cross_d_fixed=d_moving.cross(d_fixed);
      e_x=e_p.dot(d_moving_cross_d_fixed);
      J_x.block<1,3>(0,0)=-d_moving_cross_d_fixed.transpose() * p_moving_cross;
      J_x.block<1,3>(0,3)=e_p.transpose()* srrg_core::skew((Eigen::Vector3f)d_fixed)*d_moving_cross;
    }
  }

  // generic constraint linearization
  // side effect on _H and _b
  void IterativeRegistrationSolver::linearizeConstraint(Constraint& constraint)
  {

    using namespace std;
    const MatchablePtr& moving = constraint.moving;
    const MatchablePtr& fixed = constraint.fixed;

    Eigen::Matrix3f fixed_weight, moving_weight, constraint_weight;
    moving->weight(moving_weight);
    fixed->weight(fixed_weight);
    constraint_weight = (moving_weight.determinant() > fixed_weight.determinant())?  moving_weight : fixed_weight; 

    // buffer variables to avoid abusing the stack
    Vector3f e_p;
    e_p.setZero();
    Vector3f e_d;
    e_d.setZero();
    float e_o = 0.f;
    float e_x = 0.f;
    Matrix3_6f J_p;
    J_p.setZero();
    Matrix3_6f J_d;
    J_d.setZero();
    Matrix1_6f J_o;
    J_o.setZero();
    Matrix1_6f J_x;
    J_x.setZero();
    


    // the following variable will contain
    // what we have to do to linearize this constraint
    SelectionMatrixEntry entry;

    // we handle differently the intersection constraint,
    // that is not contained in the action matrix
    if (constraint.is_intersection){
      assert(moving->type()==Matchable::Line&&
             moving->type()==Matchable::Line&&
             "IterativeRegistrationSolver::linearizeConstraint(): " &&
             "for an intersection constraint" &&
             "the types should be both lines");
      entry.compute_p=false;
      entry.compute_d=false;
      entry.compute_o=false;
      entry.compute_x=true;
      entry.rotate_omega=false;
    } else {
      // fetch on the action matrix which parts of the jacobians
      // we have to compute
      // and how to deal with the omegas
      entry=_selection_matrix[moving->dof()][fixed->dof()];
    }
    
    /*


      cerr << "entry, selectin_matrix" << endl;
      cerr << "compute_p: "<< entry.compute_p << " ";
      cerr << "compute_d: "<< entry.compute_d << " ";
      cerr << "compute_o: "<< entry.compute_o << " ";
      cerr << "compute_x: "<< entry.compute_x << " ";
      cerr << "rotate:    "<< entry.rotate_omega << endl;;

    */

    //compute the jacobian
    errorAndJacobian(e_p, e_d, e_o, e_x,
                     J_p, J_d, J_o, J_x,
                     entry.compute_p,
                     entry.compute_d,
                     entry.compute_o,
                     entry.compute_x,
                     fixed,
                     moving,
                     entry.rotate_omega);

    // std::cerr << "ep : " << e_p.transpose() << std::endl;
    // std::cerr << "ed : " << e_d.transpose() << std::endl;
    // std::cerr << "eo : " << e_o << std::endl;
    // std::cerr << "ex : " << e_x << std::endl;


    // std::cerr << "Jp :\n" << J_p << std::endl;
    // std::cerr << "Jd :\n" << J_d << std::endl;
    // std::cerr << "Jo :\n" << J_o << std::endl;
    // std::cerr << "Jx :\n" << J_x << std::endl;
    
    // handle point term
    constraint.resetErrors();
    // temporary variables to hold the updates
    Matrix6f H_temp=Matrix6f::Zero();
    Vector6f b_temp=Vector6f::Zero();
    bool is_inlier=true;
    if (entry.compute_p) {
//      Matrix3f omega_p=fixed->directionMatrix();
      Matrix3f omega_p=fixed->omega();

      // this is to handle the case when we have to rotate the omega matrix
      // during the iterations
      /*if (entry.rotate_omega) {
        const Eigen::Matrix3f& R=_T.linear();
        omega_p=R*moving->directionMatrix()*R.transpose();
      }
      */
      if(_use_support_weight  && (fixed->type() == Matchable::Point)){
        omega_p += constraint_weight;
      }

      constraint.error_p=e_p.transpose()*omega_p*e_p;
      if(constraint.error_p > _point_kernel_threshold) {
        float scale=sqrt(_point_kernel_threshold/constraint.error_p);
        omega_p*=scale;
        is_inlier=false;
      }
      
      H_temp+=J_p.transpose()*omega_p*J_p;
      b_temp+=J_p.transpose()*omega_p*e_p;
    }
    //    std::cerr << "P:"<<std::endl << H_temp << std::endl << b_temp.transpose() << std::endl;
    
    // handle the direction term
    if (entry.compute_d){

      Eigen::Matrix3f omega_d=_omega_d;
      if(_use_support_weight && (fixed->type() != Matchable::Point)){
        omega_d += constraint_weight;
      }
      constraint.error_d=e_d.transpose()*omega_d*e_d;
      if(constraint.error_d>_direction_kernel_threshold) {
        float scale=sqrt(_direction_kernel_threshold/constraint.error_d);
        omega_d*=scale;
        is_inlier=false;
      }
      H_temp+=J_d.transpose()*omega_d*J_d;
      b_temp+=J_d.transpose()*omega_d*e_d;
    }

    //std::cerr << "D:"<<std::endl << H_temp << std::endl << b_temp.transpose() << std::endl;
    // handle the orthogonality term
    if (entry.compute_o){
      float omega_o=_omega_o;
      if(_use_support_weight  && (fixed->type() != Matchable::Point)){
        omega_o += constraint_weight(0,0);
      }
      constraint.error_o=e_o*omega_o*e_o;
      if(constraint.error_o>_orthogonal_kernel_threshold) {
        float scale=sqrt(_orthogonal_kernel_threshold/constraint.error_o);
        omega_o*=scale;
        is_inlier=false;
      }
      H_temp+=J_o.transpose()*omega_o*J_o;
      b_temp+=J_o.transpose()*omega_o*e_o;
    }
    //    std::cerr << "O:"<<std::endl << H_temp << std::endl << b_temp.transpose() << std::endl;
    // handle the intersection term
    if (entry.compute_x){
      constraint.error_x=e_x*_omega_x*e_x;
      float omega_x=_omega_x;
      if(constraint.error_x>_intersection_kernel_threshold) {
        float scale=sqrt(_intersection_kernel_threshold/constraint.error_x);
        omega_x*=scale;
        is_inlier=false;
      }
      H_temp+=J_x.transpose()*omega_x*J_x;
      b_temp+=J_x.transpose()*omega_x*e_x;
    }
    //    std::cerr << "X:"<<std::endl << H_temp << std::endl << b_temp.transpose() << std::endl;

    //    std::cerr << "CRISTO:"<<std::endl << _H << std::endl << _b.transpose() << std::endl;
    if (is_inlier || !_ignore_outliers){
      
      _H+=H_temp;
      _b+=b_temp;

      _num_inliers++;
      _chi_square += constraint.error_p + constraint.error_d + constraint.error_o + constraint.error_x;
    }
    //    std::cerr << "I/O:"<<std::endl << _H << std::endl << _b.transpose() << std::endl;
  }

  void IterativeRegistrationSolver::init(const Eigen::Isometry3f& transform_,
                                         const bool use_support_weight) {
    BaseRegistrationSolver::init(transform_, use_support_weight);
    _H.setZero();
    _b.setZero();
  }
  
  void IterativeRegistrationSolver::oneRound(){
    _H.setZero();
    _b.setZero();

    _num_inliers = 0;
    _chi_square = 0;

    //    int constraint_id = 0;
    for (Constraint& constraint:_constraints) {
      //      std::cerr << "********* Constraint #" << constraint_id++ << std::endl;
      linearizeConstraint(constraint);
    }
    _H+=Matrix6f::Identity()*_damping;
    Vector6f dx=_H.ldlt().solve(-_b);
    _T=srrg_core::v2tEuler(dx)*_T;
  }

}
