#pragma once

#include <srrg_types/defs.h>
#include <srrg_image_utils/depth_utils.h>
#include "shapes_aligner.h"

#include "opencv2/core/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <srrg_matchable_detector/matchable_detector.h>

namespace srrg_sasha{

  
  class ShapesTracker{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    struct Config{
      float point_distance_threshold = .1f; // merge only if the points are close
      float direction_distance_threshold = .8f; // merge only if the directions are close
      float scene_percentage_for_break = .8f; // [0;1] break if N% of the scene is not matched
      float _resolution = 0.05f;
      int _age_threshold_point = 8;  //inspired by Mirco Colosi in jail
      int _age_threshold_line  = 8; //inspired by Mirco Colosi
      int _age_threshold_plane = 20;  //inspired by Mirco Colosi
      int _age_threshold_surfel = 8;  //inspired by Mirco Colosi in jail      
    };
    struct TimeStats{
      double swap_and_update = -1.f;
      double sfrondation = -1.f;
      double voxelize_surfel = -1.f;
      void print() const;
      std::string write() const;
    };
    
    Config& mutableConfig(){return *_config_ptr;}
    const TimeStats& timeStats() const {return _time_stats;}
    
    ShapesTracker();

    ~ShapesTracker() {
      std::cerr << FRED("[TRACKER] destroying") << std::endl;
      delete _config_ptr;
      delete _scene_fixed;
      delete _scene_moving;
      std::cerr << BOLD(FRED("[TRACKER] destroying")) << std::endl;
    }
    
    void setup();
    void setImages(const srrg_core::RGBImage& rgb_image, const srrg_core::RawDepthImage& depth_image);

    void setVerbosity(bool verbose_){
      _verbose = verbose_;
      _shapes_aligner.setVerbosity(verbose_);
    }
    bool verbosity(){return _verbose;}

    bool promotedInMap(const srrg_matchable::MatchablePtr& matchable_);
    
    void compute();

    srrg_matchable::Scene* sceneFixed(){return _scene_fixed;}
    srrg_matchable::Scene* sceneMoving(){return _scene_moving;}
    srrg_matchable::Scene* matchableStore(){return _matchable_store;}
    
    const ShapesAligner& aligner() const {return _shapes_aligner;}
    ShapesAligner& aligner() {return _shapes_aligner;}

    const Eigen::Isometry3f& globalT(){return _globalT;}

    const srrg_matchable_detector::MatchableDetector& detector() const {return _detector;}
    srrg_matchable_detector::MatchableDetector& detector(){return _detector;}

    void countMatchables(const std::string& filename);

  protected:
    bool _verbose;
    Config* _config_ptr = nullptr;
    TimeStats _time_stats;
    
    bool _fixed_set;
    bool _moving_set;
    srrg_matchable::Scene* _scene_fixed;
    srrg_matchable::Scene* _scene_moving;
    srrg_matchable::Scene* _matchable_store;
    
    Eigen::Isometry3f _globalT,_inv_globalT;
    ShapesAligner _shapes_aligner;

    srrg_matchable_detector::MatchableDetector _detector;
  private:
    void recoverFromBrokenTrack();

    void swapScenesAndUpdateMatchableStore();

    void sfrondation();
    void lineSfrondation();
    void voxelizeSurfels();
    
    bool _track_broken;

    int _seq = 0;

  };
}
