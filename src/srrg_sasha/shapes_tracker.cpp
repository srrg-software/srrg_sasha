#include "shapes_tracker.h"

using namespace srrg_core;
using namespace srrg_matchable;
using namespace srrg_matchable_detector;

namespace srrg_sasha{

  void ShapesTracker::TimeStats::print() const {
    std::cerr << FYEL("Tracker Time Stats") << std::endl;
    std::cerr << "swap_and_update:       " << swap_and_update << std::endl;
    std::cerr << "|" << std::endl;
    std::cerr << "|---> sfrondation:     " << sfrondation << std::endl;    
    std::cerr << "|---> voxelize surfel: " << voxelize_surfel << std::endl;    
  }

  std::string ShapesTracker::TimeStats::write() const {
    std::stringstream stats;
    stats << "Tracker Time Stats" << std::endl;
    stats << "swap_and_update:       " << swap_and_update << std::endl;
    return stats.str();
  }
  
  ShapesTracker::ShapesTracker(){
    _config_ptr = new Config;
    
    _scene_fixed = new Scene();
    _scene_moving = new Scene();
    _matchable_store = new Scene();
    _globalT = Eigen::Isometry3f::Identity();
    _inv_globalT = Eigen::Isometry3f::Identity();

    _fixed_set = false;
    _moving_set = false;

    _shapes_aligner.setMaxIterations(20);

    _verbose = false;
    _track_broken = false;
  }

  void ShapesTracker::setup(){

  }

  void ShapesTracker::setImages(const srrg_core::RGBImage &rgb_image,
                                const srrg_core::RawDepthImage &depth_image){

    Scene matchables;
    _detector.setImages(depth_image,rgb_image);
    matchables.clear();
    _detector.compute(matchables);
    const int num_matchables = matchables.size();

    bool populate_fixed = false;
    if(!_fixed_set){
      populate_fixed = true;
      _scene_fixed->resize(num_matchables);
      _fixed_set = true;
    } else {
      _scene_moving->clear();
      _scene_moving->resize(num_matchables);
      _moving_set = true;
    }

    for(size_t i=0; i < matchables.size(); ++i){
      MatchablePtr current = matchables[i];
      if(populate_fixed)
        (*_scene_fixed)[i] = current;
      else
        (*_scene_moving)[i] = current;

      /*
      if(current->type() == Matchable::Plane){
        char filename[100];
        sprintf(filename, "plane-%07d.cloud", (int)i);
        std::ofstream outfile;
        outfile.open(filename);
        current->cloud().write(outfile);
        outfile.close();
      }
      */
    }

    _seq++;

  }

  bool ShapesTracker::promotedInMap(const MatchablePtr& matchable_) {
    switch(matchable_->type()) {
    case Matchable::Point:
      return matchable_->age() >= _config_ptr->_age_threshold_point;
      break;
    case Matchable::Line:
      return matchable_->age() >= _config_ptr->_age_threshold_line;
      break;
    case Matchable::Plane:
      return matchable_->age() >= _config_ptr->_age_threshold_plane;
      break;
    case Matchable::Surfel:
      return matchable_->age() >= _config_ptr->_age_threshold_surfel;
      break;
    default:
      throw std::runtime_error("[ShapesTracker][promotedInMap]: unknown matchable type");
      break;
    }
  }
  

  // manages the situation of track broken, i.e.
  // the fixed is cleared and the moving becomes the new fixed
  void ShapesTracker::recoverFromBrokenTrack() {
    Scene* fixed_to_clear = _scene_fixed;
    _scene_fixed = _scene_moving;
    _scene_moving = new Scene();
    fixed_to_clear->clear();
    delete fixed_to_clear;
    _matchable_store->clear();
    _moving_set = false;
    _track_broken = false;
  }
  void analScene(Scene* scene_){
     const int scene_size = scene_->size();
     int points = 0, lines = 0, planes = 0, surfels = 0, wtf = 0;
     for(int i=0; i < scene_size; ++i){
       Matchable::Type type = ((*scene_)[i])->type();
       if(type == Matchable::Point)
         points++;
       else if(type == Matchable::Line)
         lines++;
       else if(type == Matchable::Plane)
         planes++;
       else if(type == Matchable::Surfel)
         surfels++;
       else
         wtf++;
     }
     std::cerr << FBLU("[ANAL][Scene]: this scene has: ") << scene_size
               << FBLU(" matchables {points: ") << points
               << FBLU(" / lines: ") << lines
               << FBLU(" / planes: ") << planes
               << FBLU(" / surfels: ") << surfels
               << FBLU(" / wtf: ") << wtf
               << FBLU(" }") << std::endl;
   }

  void ShapesTracker::countMatchables(const std::string & filename){
    int points = 0;
    int lines = 0;
    int planes = 0;
    int surfels = 0;
    int wtf = 0;
    const int scene_size = _matchable_store->size();

    for(int i=0; i < scene_size; ++i){
      Matchable::Type type = _matchable_store->at(i)->type();
      if(type == Matchable::Point)
        points++;
      else if(type == Matchable::Line)
        lines++;
      else if(type == Matchable::Plane)
        planes++;
      else if(type == Matchable::Surfel)
        surfels++;
      else
        wtf++;
    }

    std::ofstream counter(filename);
    counter << "\n[ANAL][Scene]: this scene has: " << scene_size
      << " matchables {points: " << points
      << " / lines: " << lines
      << " / planes: " << planes
      << " / surfels: " << surfels
      << " / wtf: " << wtf
      << " }" << std::endl;
    counter.close();
  }

  void ShapesTracker::swapScenesAndUpdateMatchableStore(){
    using namespace std;
    size_t dest_idx=0;
    if(!_fixed_set || !_moving_set)
      throw std::runtime_error("[TRACKER][mergeScenes] scenes not initialized");

    const int _scene_moving_size = _scene_moving->size();
    
    int added = 0, merged = 0, unass = 0, uage = 0;
    int pt_added = 0, pt_merged = 0, pt_unass = 0, pt_uage = 0;
    int ln_added = 0, ln_merged = 0, ln_unass = 0, ln_uage = 0;
    int pl_added = 0, pl_merged = 0, pl_unass = 0, pl_uage = 0;
    int sf_added = 0, sf_merged = 0, sf_unass = 0, sf_uage = 0;
    
    // Updating matchable storing with moving
    for(size_t i = 0; i < _scene_moving->size(); ++i) {
      MatchablePtr moving = (*_scene_moving)[i];
      _scene_moving->at(dest_idx)=moving;
      
      int association_id = moving->associationID();
      // CASE 1: Un-associated. We keep them as they are
      if(association_id == -1 || !moving->isGood()){
        unass++;
        switch(moving->type()){
        case Matchable::Point: ++pt_unass; break;
        case Matchable::Line: ++ln_unass; break;
        case Matchable::Plane: ++pl_unass; break;
        case Matchable::Surfel: ++sf_unass; break;
        }
        moving->cumP() += _globalT*moving->point();
        moving->cumD() += _globalT.linear()*moving->rotation().col(0);
        moving->age() = 1;
        ++dest_idx;
        continue;
      }

      //If the association is correct, we increment the age      
      MatchablePtr fixed_associated = _scene_fixed->at(association_id);
      moving->age() = fixed_associated->age() + 1;
      moving->cumP() = fixed_associated->cumP();
      moving->cumD() = fixed_associated->cumD();

            
      // CASE 2: In-Map matchables. We merge them in the map storage
      if(fixed_associated->mapId() != -1){
        moving->mapId() = fixed_associated->mapId();
        
        MatchablePtr map_item = _matchable_store->at(moving->mapId());
        map_item->merge(moving,_globalT);
        
        merged++;
        switch(moving->type()){
        case Matchable::Point: ++pt_merged; break;
        case Matchable::Line: ++ln_merged; break;
        case Matchable::Plane: ++pl_merged; break;
        case Matchable::Surfel: ++sf_merged; break;
        }
        _scene_moving->at(i)=nullptr;
        continue;
      }

      // CASE 3: Re-observed, but not in the map.
      if(promotedInMap(moving)
         //         moving->age() >= _config_ptr->_age_threshold
         && moving->isGood()){

        moving->transformInPlace(_globalT);
        moving->mapId() = _matchable_store->size();
        moving->update();
        _matchable_store->push_back(moving);
        added++;
        switch(moving->type()){
        case Matchable::Point: ++pt_added; break;
        case Matchable::Line: ++ln_added; break;
        case Matchable::Plane: ++pl_added; break;
        case Matchable::Surfel: ++sf_added; break;
        }
        _scene_moving->at(i)=nullptr;
      } else {       // CASE 4: Re-observed, but not in the map and underage
        uage++;
        switch(moving->type()){
        case Matchable::Point: ++pt_uage; break;
        case Matchable::Line: ++ln_uage; break;
        case Matchable::Plane: ++pl_uage; break;
        case Matchable::Surfel: ++sf_uage; break;
        }
        ++dest_idx;

        moving->cumP() += _globalT*moving->point();
        moving->cumD() += _globalT.linear()*moving->rotation().col(0);
      } 
    }
    
    _scene_moving->resize(dest_idx);
    _scene_fixed->clear();

    _time_stats.sfrondation = srrg_core::getTime();
    sfrondation();
//    lineSfrondation();
    _time_stats.sfrondation = srrg_core::getTime() - _time_stats.sfrondation;

    // _time_stats.voxelize_surfel = srrg_core::getTime();
    // voxelizeSurfels();
    // _time_stats.voxelize_surfel = srrg_core::getTime() - _time_stats.voxelize_surfel;
    
    for(size_t i = 0; i < _matchable_store->size(); ++i) {
      
      MatchablePtr current(_matchable_store->at(i)->clone());

      if(current->mapId() == -1){
        std::cerr << "fuck - " << endl;
      }
      
      current->transformInPlace(_globalT.inverse());
      _scene_fixed->push_back(current);
    }
    int num_moving_cativi=0;
    for(size_t i = 0; i < _scene_moving->size(); ++i){
      MatchablePtr current = _scene_moving->at(i);
      if (!current) {
        ++num_moving_cativi;
        continue;
      }
      _scene_fixed->push_back(current);
    }

    
    if(_verbose) {
      std::cerr << std::endl;
      std::cerr << FGRN("[TRACKER][mergeScenes] merged: ") << merged << FGRN(" {points: ") << pt_merged
                << FGRN(" / lines: ") << ln_merged << FGRN(" / planes: ") << pl_merged << FGRN(" / surfels: ") << sf_merged<< FGRN(" }") << std::endl;
      std::cerr << FGRN("[TRACKER][mergeScenes] added: ") << added << FGRN(" {points: ") << pt_added
                << FGRN(" / lines: ") << ln_added << FGRN(" / planes: ") << pl_added << FGRN(" / surfels: ") << sf_added << FGRN(" }") << std::endl;
      std::cerr << FGRN("[TRACKER][mergeScenes] unass: ") << unass << FGRN(" {points: ") << pt_unass
                << FGRN(" / lines: ") << ln_unass << FGRN(" / planes: ") << pl_unass << FGRN(" / surfels: ") << sf_unass << FGRN(" }") << std::endl;
      std::cerr << FGRN("[TRACKER][mergeScenes] uage: ") << uage << FGRN(" {points: ") << pt_uage
                << FGRN(" / lines: ") << ln_uage << FGRN(" / planes: ") << pl_uage << FGRN(" / surfels: ") << sf_uage << FGRN(" }") << std::endl << std::endl;
      std::cerr << FCYN("[TRACKER][mergeScenes]: matchable store ");
      analScene(_matchable_store);
      std::cerr << FCYN("[TRACKER][mergeScenes]: moving ");
      analScene(_scene_moving);
      std::cerr << FCYN("[TRACKER][mergeScenes]: fixed ");
      analScene(_scene_fixed);
    }
  }


  void ShapesTracker::lineSfrondation(){
    Scene* backup = new Scene;
    int num_matchables = _matchable_store->size();
    backup->resize(num_matchables,nullptr);
    int k=0;
    for(int i=0; i < num_matchables; ++i){
      MatchablePtr& matchable_ref = _matchable_store->at(i);

      if(!matchable_ref)
        continue;

      if(matchable_ref->type() != Matchable::Line){
        matchable_ref->mapId() = k;
        backup->at(k) = matchable_ref;
        k++;
        continue;
      }

      for(int j=i+1; j < num_matchables; ++j){
        const MatchablePtr& matchable_cur = _matchable_store->at(j);

        if(!matchable_cur || matchable_cur->type() != Matchable::Line)
          continue;


        double dist = cv::norm( matchable_ref->descriptor(),
                                matchable_cur->descriptor(),
                                cv::NORM_HAMMING);

        if(dist > 20)
          continue;

        matchable_ref->merge(matchable_cur);

        _matchable_store->at(j) = nullptr;
      }

      matchable_ref->mapId() = k;
      backup->at(k) = matchable_ref;
      k++;
    }
    backup->resize(k);

    _matchable_store = backup;
  }

  void ShapesTracker::sfrondation(){
    Scene* backup = new Scene;
    int num_matchables = _matchable_store->size();
    backup->resize(num_matchables,nullptr);
    int k=0;
    for(int i=0; i < num_matchables; ++i){
      MatchablePtr& matchable_ref = _matchable_store->at(i);

      if(!matchable_ref)
        continue;
      
      if(matchable_ref->type() != Matchable::Plane){
        matchable_ref->mapId() = k;
        backup->at(k) = matchable_ref;
        k++;
        continue;
      }
     
      for(int j=i+1; j < num_matchables; ++j){
        const MatchablePtr& matchable_cur = _matchable_store->at(j);

        if(!matchable_cur || matchable_cur->type() != Matchable::Plane)
          continue;

        const float direction_dist = matchable_ref->rotation().col(0).dot(matchable_cur->rotation().col(0));
        if(direction_dist < 0.8)
          continue;

        const float plane_dist = fabs(matchable_ref->rotation().col(0).dot(matchable_ref->point())-matchable_cur->rotation().col(0).dot(matchable_cur->point()));
        if(plane_dist > 0.15f)
          continue;

        matchable_ref->merge(matchable_cur);

        _matchable_store->at(j) = nullptr;
      }
      
      matchable_ref->mapId() = k;
      backup->at(k) = matchable_ref;
      k++;
    }
    backup->resize(k);
    
    _matchable_store = backup;
  }

  void ShapesTracker::voxelizeSurfels(){
    if(!_matchable_store->size())
      return;

    const int num_matchables = _matchable_store->size();

    Scene* backup = new Scene;
    backup->resize(num_matchables,nullptr);
    
    int j=0;
    Cloud3D surfel_cloud;
    surfel_cloud.resize(num_matchables);
    int n=0;
    for (size_t i= 0; i < num_matchables; ++i) {

      MatchablePtr& matchable_ref = _matchable_store->at(i);

      if(!matchable_ref)
        continue;

      if(matchable_ref->type() != Matchable::Surfel){
        matchable_ref->mapId() = j;
        backup->at(j) = matchable_ref;
        j++;
        continue;
      }

      surfel_cloud[n] = RichPoint3D(matchable_ref->point(),
                                    matchable_ref->rotation().col(0),
                                    1.0f);
      n++;
    }
    surfel_cloud.resize(n);
    surfel_cloud.voxelize(_config_ptr->_resolution);

    for(int i=0; i < surfel_cloud.size(); ++i){
      const RichPoint3D& surfel = surfel_cloud[i];

      MatchablePtr surfel_matchable (new Matchable(Matchable::Surfel,
                                                   surfel.point()));
      surfel_matchable->setRotation(surfel.normal());
      surfel_matchable->age() = _config_ptr->_age_threshold_surfel;
      surfel_matchable->mapId() = i+j;
      backup->at(i+j) = surfel_matchable;
    }
    
    backup->resize(j+surfel_cloud.size());

    _matchable_store = backup;
  }
  
  void ShapesTracker::compute() {
    if(!_fixed_set || !_moving_set)
      return;

    _shapes_aligner.setFixedScene(_scene_fixed);
    _shapes_aligner.setMovingScene(_scene_moving);

    _shapes_aligner.compute(Eigen::Isometry3f::Identity());

    const Eigen::Isometry3f& deltaT = _shapes_aligner.T();
    
    _globalT =_globalT*deltaT;

    _inv_globalT = _globalT.inverse();

    if(_verbose){
      std::cerr << FCYN("[TRACKER][compute]: analyzing fixed ");
      analScene(_scene_fixed);
      std::cerr << FCYN("[TRACKER][compute]: analyzing moving ");
      analScene(_scene_moving);
    }

    int un_associated = _shapes_aligner.nnCorrespondenceFinder().unAssociated();
    const int _scene_moving_size = _scene_moving->size();
    const int max_num_of_unass = _scene_moving_size * _config_ptr->scene_percentage_for_break;

    // if(un_associated > max_num_of_unass) {
    //   _track_broken = true;
    //   std::cerr << FRED("TRACK BROKEN!") << std::endl;
    //   recoverFromBrokenTrack();
    //   return;
    // }
    
    _scene_fixed->transformInPlace(deltaT.inverse());
    
    _time_stats.swap_and_update = srrg_core::getTime();
    swapScenesAndUpdateMatchableStore();
    _time_stats.swap_and_update = srrg_core::getTime() - _time_stats.swap_and_update;
    
  }
}
