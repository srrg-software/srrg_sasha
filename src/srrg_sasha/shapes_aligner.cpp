#include "shapes_aligner.h"

using namespace srrg_matchable;

namespace srrg_sasha{

  void ShapesAligner::TimeStats::print() const {
    std::cerr << FYEL("Aligner Time Stats") << std::endl;
    std::cerr << "data_association:   " << data_association << std::endl;
    std::cerr << "solve:              " << solve << std::endl;
  }

  std::string ShapesAligner::TimeStats::write() const {
    std::stringstream stats;
    stats << "Aligner Time Stats" << std::endl;
    stats << "data_association:   " << data_association << std::endl;
    stats << "solve:              " << solve << std::endl << std::endl;
    return stats.str();

  }
  
  void ShapesAligner::computeDirect(const Eigen::Isometry3f& init_guess_){
    _direct_solver.init(init_guess_, _configuration->use_support_weight);
    _direct_solver.setIgnoreOutliers(false);
    _nn_ass.compute(_direct_solver.constraints(), _direct_solver.transform());
    if(_configuration->use_cross_constraints)        
      _bf_ass.compute(_iterative_solver.constraints(), _iterative_solver.transform()); 
    _direct_solver.oneRound();
    _T = _direct_solver.transform();
  }

  
  void ShapesAligner::compute(const Eigen::Isometry3f& init_guess_){
    
    if(!_scene_fixed)
      throw std::runtime_error("[ALIGNER][compute]: null pointer");
    if(!_scene_moving)
      throw std::runtime_error("[ALIGNER][compute]: null pointer");

    double association_cumulative_time = 0.0;
    double solver_cumulative_time = 0.0;

    _nn_ass.setScenes(_scene_fixed, _scene_moving);
    if(_configuration->use_cross_constraints)
      _bf_ass.setScenes(_scene_fixed, _scene_moving);
    double time_association = srrg_core::getTime();
    _nn_ass.init();
    if(_configuration->use_cross_constraints)
      _bf_ass.init();
    association_cumulative_time += srrg_core::getTime() - time_association;
    

    if(_configuration->use_direct_solver){
      computeDirect(init_guess_);
      return;
    }

    _iterative_solver.init(init_guess_, _configuration->use_support_weight);
    _iterative_solver.setIgnoreOutliers(false);
    for(int i=0; i < _max_iterations; ++i){
      _iterative_solver.constraints().clear();

      time_association = srrg_core::getTime();
      _nn_ass.compute(_iterative_solver.constraints(), _iterative_solver.transform());
      if(_configuration->use_cross_constraints)        
        _bf_ass.compute(_iterative_solver.constraints(), _iterative_solver.transform()); //for cross-constraints on un-associated matchables

      //bdc, if there are not associations, break
      if(!_iterative_solver.constraints().size()) {
        std::cerr << FRED("[ALIGNER][compute]: skipping frame for 0 associations") << std::endl;
        break;
      }
      
      association_cumulative_time += srrg_core::getTime() - time_association;

      double time_iterative_solver = srrg_core::getTime();
      _iterative_solver.setIgnoreOutliers(i==_max_iterations-1);
      _iterative_solver.oneRound();
      solver_cumulative_time += srrg_core::getTime() - time_iterative_solver;

      if(_verbose  && (!i || i == _max_iterations-1)){
        std::cerr << FRED("[ALIGNER][compute] it: ") << i << " "
                  << FGRN("chi square: ") << _iterative_solver.chiSquare() << " "
                  << FGRN("num inliers: ") << _iterative_solver.numInliers() << "   "
                  << FGRN("T: ") << srrg_core::t2v(_iterative_solver.transform()).transpose() << std::endl;
      }
    }

    _T = _iterative_solver.transform();

    _time_stats.data_association = association_cumulative_time;
    _time_stats.solve = solver_cumulative_time;

    _association_timings.push_back(_time_stats.data_association);
    _solve_timings.push_back(_time_stats.solve);
    
    if(_verbose){
      std::cerr << FYEL("[ALIGNER][compute] time: ") << solver_cumulative_time+association_cumulative_time << " "
                << FYEL("{data ass: ") << association_cumulative_time << FYEL(" / solve: ") << solver_cumulative_time
                << FYEL("}") << std::endl;
    }
    //std::cerr << "Final T: " << srrg_core::t2v(_T).transpose() << std::endl;
  }

}
