#include "bf_correspondence_finder.h"

namespace srrg_sasha {

  void BFCorrespondenceFinder::init() {
    if(!_fixed || !_moving)
      throw std::runtime_error("[NNCorrespondenceFinder][init]: scenes not initialized!");    
    //nothing to init here
  }
  
  void BFCorrespondenceFinder::compute(BaseRegistrationSolver::ConstraintVector& constraints_,
                                       const Eigen::Isometry3f& T_) {
    if(!_fixed || !_moving)
      throw std::runtime_error("[NNCorrespondenceFinder][init]: scenes not initialized!");
    _un_associated = 0;

    int cross_ass  = 0,
      un_ass       = 0;

    for(size_t i_mov = 0; i_mov < _moving_size; ++i_mov) {
      // get matchable reference     
      Matchable& moving_matchable = *(_moving->at(i_mov));

      // if already associated with nn, continue
      if(!moving_matchable.association())
        continue;
      
      const Matchable::Type& type_moving = moving_matchable.type();
      if(type_moving == Matchable::Surfel)
        continue;
      
      const Eigen::Vector3f p_moving = T_ * moving_matchable.point();
      const Eigen::Matrix3f R_moving = T_.linear()*moving_matchable.rotation();
      const Eigen::Vector3f d_moving = T_.linear()*moving_matchable.rotation().col(0);
      const Eigen::Matrix3f R_moving_prime= moving_matchable.rotation().transpose();
      
      
      int best_fixed_match = -1; //index of best fixed matchable
      float best_fixed_error = FLT_MAX; //value of best match. The lower, the better
      
      for(size_t i_fix = 0; i_fix < _fixed_size; ++i_fix) {
        
        const MatchablePtr& fixed = _fixed->at(i_fix);
        const Matchable::Type& type_fixed = fixed->type();
        if(type_fixed == Matchable::Surfel ||
           type_fixed == Matchable::Point ||
           type_fixed == type_moving)
          continue;

        const Eigen::Vector3f& p_fixed = fixed->point();
        const Eigen::Matrix3f& R_fixed = fixed->rotation();
        const Eigen::Vector3f& d_fixed = fixed->rotation().col(0);
        const Eigen::Matrix3f R_fixed_prime = R_fixed.transpose();

        // point error always computed
        const Eigen::Vector3f e_pt = R_fixed_prime*(p_moving - p_fixed);
        if(type_moving == Matchable::Point ||
           type_fixed == Matchable::Point) { //we should stop here
          const float current_error = e_pt.norm();
          if(current_error < best_fixed_error) {
            best_fixed_error = current_error;
            best_fixed_match = i_fix;
          }
          continue;
        }
        // if( (type_moving == Matchable::Line && type_fixed == Matchable::Line) ||
        //     (type_moving == Matchable::Plane && type_fixed == Matchable::Plane) ) {
        //   const Eigen::Vector3f e_d = d_moving - d_fixed;
        //   const float current_error = e_pt.norm() + e_d.norm();
        //   if(current_error < best_fixed_error) {
        //     best_fixed_error = current_error;
        //     best_fixed_match = i_fix;
        //   }
        //   continue;
        // }
        
        if( (type_moving == Matchable::Line && type_fixed == Matchable::Plane) ||
            (type_moving == Matchable::Plane && type_fixed == Matchable::Line) ) {
          const float e_o = d_moving.dot(d_fixed);
          const float current_error = e_pt.norm() + fabs(e_o);
          if(current_error < best_fixed_error) {
            best_fixed_error = current_error;
            best_fixed_match = i_fix;
          }
          continue;
        }
      } //for fixed

      if(best_fixed_match == -1 || best_fixed_error > _configuration->error_threshold) { 
        ++un_ass;
        continue;
      }

      moving_matchable.setAssociation(_fixed->at(best_fixed_match), best_fixed_match);
      constraints_.push_back(BaseRegistrationSolver::Constraint(_fixed->at(best_fixed_match),_moving->at(i_mov),false));

      //bdc debugging
      cross_ass++;

      
    } //for moving

    
    if(_verbose){
      std::cerr << FCYN("[BFCORRFINDER][compute] find: ") << FCYN("  cross:  ") << cross_ass << std::endl;
    }

  }

}
