#include "data_association.h"

using namespace srrg_matchable;

namespace srrg_sasha{


  void DataAssociation::associate(Type type_,
                                  BaseRegistrationSolver::ConstraintVector &constraints_,
                                  Scene *scene_fixed_,
                                  Scene *scene_moving_,
                                  const Eigen::Isometry3f &transform_){

    _scene_associations->clear();
    
    if(type_ == BruteForce){
      associateWBruteForce(constraints_, scene_fixed_, scene_moving_, transform_);
    }
    else if(type_ == Projective){
      associateWProjection(constraints_, scene_fixed_, scene_moving_, transform_);
    }
    
  }
  
  void DataAssociation::associateWProjection(BaseRegistrationSolver::ConstraintVector &constraints,
                                             Scene *scene_fixed,
                                             Scene *scene_moving,
                                             const Eigen::Isometry3f &transform){

    // Project
    // I need a Projector, and this kind of data association
    // should be separated, otherwise we reproject multiple times the same thing.
    // ...
    // From the Gospel of Motaba 1,1
    // "Think a lot, code less"
    
  }

  
  void DataAssociation::associateWBruteForce(BaseRegistrationSolver::ConstraintVector &constraints,
                                             Scene *scene_fixed,
                                             Scene *scene_moving,
                                             const Eigen::Isometry3f &transform){
    const int fixed_size = scene_fixed->size();
    const int moving_size = scene_moving->size();

    _scene_associations->clear();

    for(int i=0; i < fixed_size; ++i){
      MatchablePtr fixed = (*scene_fixed)[i];
      const Matchable::Type& fixed_type = fixed->type();
      MatchablePtr moving_best = nullptr;
      float e_p_best = std::numeric_limits<float>::max();
      for(int j=0; j < moving_size; ++j){
        MatchablePtr moving = (*scene_moving)[j];
        const Matchable::Type& moving_type = moving->type();

        if (fixed_type != moving_type)
          continue;

        Eigen::Vector3f e_p = transform*moving->point()-fixed->point();

        float error = e_p.transpose()*e_p;

        if(fixed_type == Matchable::Line){
          Eigen::Vector3f e_d = transform.linear()*moving->directionVector() - fixed->directionVector();
          error += e_d.transpose()*e_d;
        }

        if(error<e_p_best){
          e_p_best = error;
          moving_best = moving;
        }
      }
      if (!moving_best)
        continue;

      constraints.push_back(BaseRegistrationSolver::Constraint(fixed,moving_best,false));


      //        Eigen::Vector3f pA = fixed->point();
      //        Eigen::Vector3f pB = moving_best->point();
      //        Eigen::Vector3f p = (pA+pB)/2;
      //        Eigen::Vector3f diff = pB-pA;
      //        const Eigen::Vector3f color = Eigen::Vector3f (0.0f,1.0f,1.0f);
      //        float module = diff.norm();
      //        diff /= module;

      //        _scene_associations->push_back(MatchablePtr(new Matchable(Matchable::Line, p, diff, Eigen::Vector2f(module/2,0))));

    }
  }

}
