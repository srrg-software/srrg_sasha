#pragma once
#include <srrg_matchable/matchable.h>
#include <srrg_matchable/scene.h>
#include "base_registration_solver.h"

namespace srrg_sasha{
  class DirectRegistrationSolver : public BaseRegistrationSolver {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    // constructs an empty solver
    DirectRegistrationSolver();

    //
    ~DirectRegistrationSolver();

    // prepares the optimization, starting from the specified pose
    virtual void init(const Eigen::Isometry3f& transform_=Eigen::Isometry3f::Identity(),
                      const bool use_support_weight = false);
    
    // executes one round of optimization
    virtual void oneRound();

  protected:

    typedef Eigen::Matrix<float,12,1>  Vector12f;
    typedef Eigen::Matrix<float,12,12> Matrix12f;
    typedef Eigen::Matrix<float,6,12> Matrix6_12f;
    typedef Eigen::Matrix<float,3,12>   Matrix3_12f;
    typedef Eigen::Matrix<float,1,12>   Matrix1_12f;

    // computes the error and the Jacobian of a constraint
    inline void errorAndJacobian(Vector3f& e_p,    // point-error
                                 Vector3f& e_d,    // direction-error
                                 float& e_o,              // orthogonality error
                                 float& e_x,              // intersection error
                                 Matrix3_12f& J_p,  // point jacobian
                                 Matrix3_12f& J_d,  // direction jacobian
                                 Matrix1_12f& J_o,  // orthogonality jacobian
                                 Matrix1_12f& J_x,  // orthogonality error
                                 bool compute_p,          // if true computes the point part
                                 bool compute_d,          // if true computes the direction part
                                 bool compute_o,          // if true computes the orthogonality part
                                 bool compute_x,          // if true computes the orthogonality part
                                 const srrg_matchable::MatchablePtr fixed, // input fixed item
                                 const srrg_matchable::MatchablePtr moving); // input moving item    
    
    // generic constraint linearization
    // side effect on _H, _b and the constrant vector to write the error
    // of each constraint
    inline void linearizeConstraint(Constraint& constraint);    // set to true if the constraint is an intersection

    static inline Vector12f flattenIsometry(const Isometry3f& iso);
    static inline Isometry3f unflattenIsometry(const Vector12f& v);
    static inline Matrix6_12f jacobianT(const Vector3f& point, const Vector3f& direction);

    void oneRoundUpper();
    void oneRoundLower();
    Matrix12f _H;
    Vector12f _b;

  };
  
}
