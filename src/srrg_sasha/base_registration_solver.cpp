#include "base_registration_solver.h"
#include <limits>

namespace srrg_sasha{
  using namespace srrg_matchable;
  
  const BaseRegistrationSolver::SelectionMatrixEntry BaseRegistrationSolver::_selection_matrix[][Matchable::Dim]=
    {
      {
        {// point-point
          .compute_p=true,
          .compute_d=false,
          .compute_o=false,
          .compute_x=false,
          .rotate_omega=false
        },
        {// point-line
          .compute_p=true,
          .compute_d=false,
          .compute_o=false,
          .compute_x=false,
          .rotate_omega=false
        },
        {// point-plane
          .compute_p=true,
          .compute_d=false,
          .compute_o=false,
          .compute_x=false,
          .rotate_omega=false
        },
        {// point-surfel
          .compute_p=true,
          .compute_d=false,
          .compute_o=false,
          .compute_x=false,
          .rotate_omega=false
        }
      },
      {
        {// line-point
          .compute_p=true,
          .compute_d=false,
          .compute_o=false,
          .compute_x=false,
          .rotate_omega=true
        },
        {// line-line
          .compute_p=true,
          .compute_d=true,
          .compute_o=false,
          .compute_x=false,
          .rotate_omega=false
        },
        {// line-plane
          .compute_p=true,
          .compute_d=false,
          .compute_o=true,
          .compute_x=false,
          .rotate_omega=false
        },
        {// line-surfel
          .compute_p=true,
          .compute_d=false,
          .compute_o=true,
          .compute_x=false,
          .rotate_omega=false
        }
      },
      {
        {// plane-point
          .compute_p=true,
          .compute_d=false,
          .compute_o=false,
          .compute_x=false,
          .rotate_omega=true
        },
        {// plane-line
          .compute_p=true,
          .compute_d=false,
          .compute_o=true,
          .compute_x=false,
          .rotate_omega=true
        },
        {// plane-plane
          .compute_p=true,
          .compute_d=true,
          .compute_o=false,
          .compute_x=false,
          .rotate_omega=false
        },
        {// plane-surfel
          .compute_p=true,
          .compute_d=true,
          .compute_o=false,
          .compute_x=false,
          .rotate_omega=false
        }
      },
      {
        {//surfel-point
          .compute_p=true,
          .compute_d=false,
          .compute_o=false,
          .compute_x=false,
          .rotate_omega=true
        },
        {//surfel-line
          .compute_p=true,
          .compute_d=false,
          .compute_o=true,
          .compute_x=false,
          .rotate_omega=true
        },
        {//surfel-plane
          .compute_p=true,
          .compute_d=true,
          .compute_o=false,
          .compute_x=false,
          .rotate_omega=false
        },
        {//surfel-surfel
          .compute_p=true,
          .compute_d=true,
          .compute_o=false,
          .compute_x=false,
          .rotate_omega=false
        }
      }
    };

  BaseRegistrationSolver::Constraint::Constraint(const MatchablePtr& fixed,
                                                 const MatchablePtr& moving,
                                                 bool is_intersection){
    this->fixed=fixed;
    this->moving=moving;
    this->is_intersection=is_intersection;
  }

  void BaseRegistrationSolver::Constraint::resetErrors(){
    error_p=0;
    error_d=0;
    error_o=0;
    error_x=0;
  }

  BaseRegistrationSolver::BaseRegistrationSolver() {
    setOmegaDirection(1);
    setOmegaOrthogonal(1);
    setOmegaIntersection(1);
    setDamping(0.1);
    _point_kernel_threshold=std::numeric_limits<float>::max();
    _direction_kernel_threshold=std::numeric_limits<float>::max();
    _orthogonal_kernel_threshold=std::numeric_limits<float>::max();
    _intersection_kernel_threshold=std::numeric_limits<float>::max();
    _ignore_outliers=false;
    _T.setIdentity();
  }

  BaseRegistrationSolver::~BaseRegistrationSolver() {
    _che_cazzo_devo_fare_upper = true;
  }

  void BaseRegistrationSolver::init(const Eigen::Isometry3f& transform_,
                                    const bool use_support_weight) {
    _T=transform_;
    _use_support_weight = use_support_weight;
  }
  

}
