#include "nn_correspondence_finder.h"

namespace srrg_sasha{

#define DIR_VEC_COL 0
  
  void NNCorrespondenceFinder::model2linear() {
    if(!_fixed)
      throw std::runtime_error("[NNCorrespondenceFinder][model2linear]: null pointer");
    const int fixed_size = _fixed->size();
    //    _reference_matchables.resize(fixed_size);

    _reference_matchable_points.clear();
    _reference_matchable_lines.clear();
    _reference_matchable_planes.clear();
    _reference_matchable_surfels.clear();
    _reference_point_map.clear();
    _reference_line_map.clear();
    _reference_plane_map.clear();
    _reference_surfel_map.clear();
    
    for(size_t i = 0; i < fixed_size; ++i) {
      const Matchable::Type& type = (*_fixed)[i]->type();
      if(type == Matchable::Point) {
        _reference_matchable_points.push_back(_fixed->at(i)->point());
        _reference_point_map.push_back(i);
      }
      else if(type == Matchable::Line) {
        srrg_core::Vector6f line_stuff;
        line_stuff.head<3>() = (_fixed->at(i)->rotation().col(DIR_VEC_COL)).cross(_fixed->at(i)->point());
        line_stuff.tail<3>() = _fixed->at(i)->rotation().col(DIR_VEC_COL) * _configuration->direction_scale;
        _reference_matchable_lines.push_back(line_stuff);
        _reference_line_map.push_back(i);
      }
      else if(type == Matchable::Plane) {
        Eigen::Vector4f plane_stuff;
        plane_stuff.head<3>() = _fixed->at(i)->rotation().col(DIR_VEC_COL) * _configuration->normal_scale;
        //        plane_stuff.tail<3>() = (*_fixed)[i]->point();
        plane_stuff(3) = (_fixed->at(i)->rotation().col(DIR_VEC_COL)).dot(_fixed->at(i)->point());
        _reference_matchable_planes.push_back(plane_stuff);
        _reference_plane_map.push_back(i);
      }
      else if(type == Matchable::Surfel) {
        srrg_core::Vector6f surfel_stuff;
        surfel_stuff.head<3>() = _fixed->at(i)->point();
        surfel_stuff.tail<3>() = _configuration->normal_scale * _fixed->at(i)->rotation().col(DIR_VEC_COL);
        _reference_matchable_surfels.push_back(surfel_stuff);
        _reference_surfel_map.push_back(i);
      }
    }
  }
  
  void NNCorrespondenceFinder::init() {
    if(!_fixed || !_moving)
      throw std::runtime_error("[NNCorrespondenceFinder][init]: scenes not initialized!");

    //bdc,  linearize the _fixed data in a KDVEctor
    model2linear();

    //bdc,  create the yellow lemon tree
    if(_kd_tree_point)
      delete _kd_tree_point;
    _kd_tree_point = 0;
    _kd_tree_point = new srrg_core::KDTree<float, KDTREE_DIM_POINT>(_reference_matchable_points, _configuration->leaf_range);

    //bdc,  create the yellow lemon tree
    if(_kd_tree_line)
      delete _kd_tree_line;
    _kd_tree_line = 0;
    _kd_tree_line = new srrg_core::KDTree<float, KDTREE_DIM_LINE>(_reference_matchable_lines,  _configuration->leaf_range);

    //bdc,  create the yellow lemon tree
    if(_kd_tree_plane)
      delete _kd_tree_plane;
    _kd_tree_plane = 0;
    _kd_tree_plane = new srrg_core::KDTree<float, KDTREE_DIM_PLANE>(_reference_matchable_planes,  _configuration->leaf_range);

    //bdc,  create the yellow lemon tree
    if(_kd_tree_surfel)
      delete _kd_tree_surfel;
    _kd_tree_surfel = 0;
    _kd_tree_surfel = new srrg_core::KDTree<float, KDTREE_DIM_SURFEL>(_reference_matchable_surfels,  _configuration->leaf_range_surfel);

    
  }

  // it takes as T_, the transform of the moving w.r.t. the fixed, i.e. T_fm
  // in fact, it pre-multiplies this T_ to the moving scene
  void NNCorrespondenceFinder::compute(BaseRegistrationSolver::ConstraintVector& constraints_,
                                       const Eigen::Isometry3f& T_) {

    if(!_kd_tree_point || !_kd_tree_line || !_kd_tree_plane || !_kd_tree_surfel
       || !_fixed || !_moving)
      throw std::runtime_error("[NNCorrespondenceFinder][compute]: data structures not initialized");

    // for visualization only
    _associations->clear();

    _un_associated = 0;
    
    //bdc,  resize constraints to max size
    constraints_.resize(_moving_size);
    int constraints_count = 0;

    int ass_lines = 0, ass_points = 0, ass_planes =0, ass_surfels = 0, cross_ass = 0, hamming_discard = 0;

    const int point_tree_size = _reference_matchable_points.size();
    const int line_tree_size = _reference_matchable_lines.size();
    const int plane_tree_size = _reference_matchable_planes.size();
    const int surfel_tree_size = _reference_matchable_surfels.size();

    for(size_t i = 0; i < _moving_size; ++i) {

      Matchable& moving_matchable = *(_moving->at(i));
      moving_matchable.setAssociation(NULL);
      //bdc,  transform moving Matchable
      const Eigen::Vector3f p = T_ * moving_matchable.point();
      const Eigen::Vector3f d = T_.linear() * moving_matchable.rotation().col(DIR_VEC_COL);
      const Matchable::Type& type = moving_matchable.type();
      
      //bdc,  create query and answer
      srrg_core::KDTree<float, KDTREE_DIM_POINT>::VectorTD answer_point;
      srrg_core::KDTree<float, KDTREE_DIM_LINE>::VectorTD answer_line;
      srrg_core::KDTree<float, KDTREE_DIM_PLANE>::VectorTD answer_plane;
      srrg_core::KDTree<float, KDTREE_DIM_SURFEL>::VectorTD answer_surfel;
      
      //bdc,  query the tree
      int index_of_reference = -1, index = -1;
      float approx_distance;
      const float max_distance = 1.f;
      
      if(type == Matchable::Point) {
        srrg_core::KDTree<float, KDTREE_DIM_POINT>::VectorTD query;
        query = p;
        if(point_tree_size)
          approx_distance = _kd_tree_point->findNeighbor(answer_point, index_of_reference, query, max_distance);
        if(index_of_reference < 0)
          index = -1;
        else
          index = _reference_point_map[index_of_reference];
      } else if( type == Matchable::Line) {
        srrg_core::KDTree<float, KDTREE_DIM_LINE>::VectorTD query;
        query.head<3>() = d.cross(p);
        query.tail<3>() = d * _configuration->direction_scale;
        if(line_tree_size)
          approx_distance = _kd_tree_line->findNeighbor(answer_line, index_of_reference, query, max_distance);
        if(index_of_reference < 0)
          index = -1;
        else
          index = _reference_line_map[index_of_reference];
      } else if( type == Matchable::Plane) {
        srrg_core::KDTree<float, KDTREE_DIM_PLANE>::VectorTD query;
        query.head<3>() = d  * _configuration->normal_scale;
        // query.tail<3>() = p;
        query(3) = d.dot(p);
        if(plane_tree_size)
          approx_distance = _kd_tree_plane->findNeighbor(answer_plane, index_of_reference, query, max_distance);
        if(index_of_reference < 0)
          index = -1;
        else          
          index = _reference_plane_map[index_of_reference];
      }
      else if( type == Matchable::Surfel) {
        srrg_core::KDTree<float, KDTREE_DIM_SURFEL>::VectorTD query;
        query.head<3>() = p;
        query.tail<3>() = d * _configuration->normal_scale;
        if(surfel_tree_size)
          approx_distance = _kd_tree_surfel->findNeighbor(answer_surfel, index_of_reference, query, max_distance);
        if(index_of_reference < 0)
          index = -1;
        else          
          index = _reference_surfel_map[index_of_reference];
      }
      
      //bdc,  check if answer satisfy us (if u know what i mean lol)
      if(index < 0 || type != _fixed->at(index)->type()){ // no match
        _un_associated++;
        continue;
      }

      // if Points, check the descriptors
      if( type == Matchable::Point){
        double dist = cv::norm( _fixed->at(index)->descriptor(),
                                _moving->at(i)->descriptor(),
                                cv::NORM_HAMMING);
        if(dist > _configuration->hamming_point_threshold){
          hamming_discard++;
          _un_associated++;
          //if(dist < 50)
          _moving->at(i)->setIsGood(false);
          continue;
        }
      }
      else if(type == Matchable::Line){
        double dist = cv::norm( _fixed->at(index)->descriptor(),
                                _moving->at(i)->descriptor(),
                                cv::NORM_HAMMING);
        if(dist > _configuration->hamming_line_threshold){
          hamming_discard++;
          _un_associated++;
          //if(dist < 50)
          _moving->at(i)->setIsGood(false);
          continue;
        }
      }
      else if(type == Matchable::Plane){
        double dist = (_fixed->at(index)->rotation().col(DIR_VEC_COL)).dot(_moving->at(i)->rotation().col(DIR_VEC_COL));
        //        double centroid_dist = (_fixed->at(index)->point() - _moving->at(i)->point()).dot(_moving->at(i)->directionVector());
        double dist_d = std::fabs((_fixed->at(index)->rotation().col(DIR_VEC_COL)).dot(_fixed->at(index)->point()) - (_moving->at(i)->rotation().col(DIR_VEC_COL)).dot(_moving->at(i)->point()));
        if(dist < 0.8 || dist_d > 0.1
           // || centroid_dist > 0.05
           ){
          //if(dist < 50)
          _un_associated++;
          _moving->at(i)->setIsGood(false);
          continue;
        }
      }
      else if( type == Matchable::Surfel){
        float dist = (_fixed->at(index)->point() - _moving->at(i)->point()).squaredNorm();
        if(dist > 0.05){
          hamming_discard++;
          _un_associated++;
          //if(dist < 50)
          _moving->at(i)->setIsGood(false);
          continue;
        }
        // double dist = cv::norm( _fixed->at(index)->descriptor(),
        //                         _moving->at(i)->descriptor(),
        //                         cv::NORM_HAMMING);
        // if(dist > _configuration->hamming_point_threshold){
        //   hamming_discard++;
        //   _un_associated++;
        //   //if(dist < 50)
        //   _moving->at(i)->setIsGood(false);
        //   continue;
        // }
      }
      
      //bdc,  add constraint
      // first to the matchable. It will be useful for merging
      moving_matchable.setAssociation(_fixed->at(index), index);
      
      // then to the constraints' set
      constraints_[constraints_count] = BaseRegistrationSolver::Constraint(_fixed->at(index),_moving->at(i),false);
      constraints_count++;

      //bdc debugging
      if(_fixed->at(index)->type() == Matchable::Point && _moving->at(i)->type() == Matchable::Point)
        ass_points++;
      else if(_fixed->at(index)->type() == Matchable::Line && _moving->at(i)->type() == Matchable::Line)
        ass_lines++;
      else if(_fixed->at(index)->type() == Matchable::Plane && _moving->at(i)->type() == Matchable::Plane)
        ass_planes++;
      else if(_fixed->at(index)->type() == Matchable::Surfel && _moving->at(i)->type() == Matchable::Surfel)
        ass_surfels++;
      else
        cross_ass++;
      
      //bdc,  for visualization add constraints to a scene
//      const Eigen::Vector3f pA = _fixed->at(index)->point();
//      const Eigen::Vector3f pB = _moving->at(i)->point();
//      const Eigen::Vector3f mean = (pA+pB)/2;
//      const Eigen::Vector3f color = Eigen::Vector3f (0.0f,0.0f,1.0f);
//      Eigen::Vector3f diff = pB-pA;
//      float module = diff.norm();
//      diff /= module;
//      _associations->push_back(MatchablePtr(new Matchable(Matchable::Line, pA, diff, Eigen::Vector2f(module,0.f))));
      
    }

    constraints_.resize(constraints_count);

    // // if(_verbose){
    // //   std::cerr << FCYN("[NNCORRFINDER][compute] find: ") << constraints_count << FCYN(" correspondences. Of which:");
    // //   std::cerr << FCYN("point-point: ") << ass_points
    // //             << FCYN("  line-line:   ") << ass_lines
    // //             << FCYN("  plane-plane:   ") << ass_planes
    // //             << FCYN("  surfel-surfel:   ") << ass_surfels
    // //             << FCYN("  point-line:  ") << cross_ass << std::endl;
    // //   std::cerr << FCYN("  - un_ass:    ") << _un_associated;
    // //             << FCYN("  hamming_discarded: ") << hamming_discard << std::endl;
    // // }
    
  }
  
}
